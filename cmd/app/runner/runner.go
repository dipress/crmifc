package runner

import (
	"fmt"
	"os"

	"github.com/romanyx/stack"
)

type Server interface {
	Start() error
	Shutdown()
	Address() string
}

// Logger allows to log server.
type Logger interface {
	Fatal(err error, extra map[string]interface{})
	Info(info string, extra map[string]interface{})
}

// Run allows to run server and stop it gracefully.
func Run(shutdown <-chan os.Signal, logger Logger, servers ...Server) {
	// Make a channel for errors.
	errChan := make(chan error)

	for _, s := range servers {
		s := s
		go func() {

			logger.Info(fmt.Sprintf("starting  %s server", s.Address()), nil)
			if err := s.Start(); err != nil {
				errChan <- stack.Errorf("launch server %s: %w", s.Address(), err)
			}
		}()

	}

	select {
	case err := <-errChan:
		logger.Fatal(err, nil)
	case <-shutdown:
		for _, s := range servers {
			s.Shutdown()
		}
	}

}
