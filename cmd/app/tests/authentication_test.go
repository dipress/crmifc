package tests

import (
	"context"
	"testing"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/dipress/crmifc/internal/proto"
	"gitlab.com/dipress/crmifc/internal/storage"
	"gitlab.com/dipress/crmifc/internal/storage/postgres"
	"google.golang.org/grpc/codes"
)

func TestAuthentication(t *testing.T) {
	t.Run("ok", authOK)
	t.Run("validation", authValidations)
	t.Run("email", authEmail)
	t.Run("password", authPassword)
}

func authOK(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	registrationSuite(t, c)

	client := proto.NewAuthClient(c.connection(t))

	req := proto.SignInRequest{
		Email:    "johndo@example.com",
		Password: "Password12345!",
	}

	tokens, err := client.SignIn(context.Background(), &req)
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	assertTokens(t, tokens)
}

func authValidations(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	registrationSuite(t, c)

	client := proto.NewAuthClient(c.connection(t))

	_, err := client.SignIn(context.Background(), new(proto.SignInRequest))
	assertValidation(t, err, map[string]string{
		"email":    "cannot be blank",
		"password": "cannot be blank",
	})
}

func authEmail(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	registrationSuite(t, c)

	client := proto.NewAuthClient(c.connection(t))

	req := proto.SignInRequest{
		Email:    "unknown@example.com",
		Password: "Password12345!",
	}

	_, err := client.SignIn(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)
}

func authPassword(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	registrationSuite(t, c)

	client := proto.NewAuthClient(c.connection(t))

	req := proto.SignInRequest{
		Email:    "unknown@example.com",
		Password: "wrongpassword",
	}

	_, err := client.SignIn(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)
}

func authSuite(t testing.TB, c *config) *proto.TokensResponse {
	registrationSuite(t, c)

	client := proto.NewAuthClient(c.connection(t))

	req := proto.SignInRequest{
		Email:    "johnDo@example.com",
		Password: "Password12345!",
	}

	tokens, err := client.SignIn(context.Background(), &req)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	return tokens
}

func registrationSuite(t testing.TB, c *config) {
	ctx, cancel := context.WithTimeout(context.Background(), caseTimeout)
	defer cancel()

	roleDAO := postgres.NewRole(c.postgres)

	role, err := roleDAO.ByName(ctx, "Admin")
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	userDAO := postgres.NewUser(c.postgres)

	passwordHash, err := c.authenticator.PasswordHash("Password12345!")
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	newUser := storage.User{
		ID:           uuid.NewV4().String(),
		RoleID:       role.ID,
		Email:        "johnDo@example.com",
		PasswordHash: passwordHash,
		FirstName:    "John",
		LastName:     "Do",
	}

	if err := userDAO.Store(ctx, &newUser); err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
}
