package tests

import (
	"context"
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/dipress/crmifc/internal/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/emptypb"
)

func TestCreatePermission(t *testing.T) {
	t.Run("ok", permissionOK)
	t.Run("unauthorized", permissionCreateUnauthorized)
	t.Run("validation", permissionCreateValidation)
}

func permissionOK(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	cred := authSuite(t, c)

	client := proto.NewPermissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(cred.AccessToken)),
	))

	req := proto.PermissionCreateRequest{
		Service:     "proto.MyService",
		Method:      "Create",
		Description: "Create a new my service",
	}

	resp, err := client.Create(context.Background(), &req)
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	assert.Equal(t, "proto.MyService", resp.Service)
	assert.Equal(t, "Create", resp.Method)
	assert.Equal(t, "Create a new my service", resp.Description)
}

func permissionCreateUnauthorized(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	client := proto.NewPermissionClient(c.connection(t))

	req := proto.PermissionCreateRequest{
		Service:     "proto.MyService",
		Method:      "Create",
		Description: "Create a new my service",
	}

	_, err := client.Create(context.Background(), &req)
	assertCode(t, err, codes.Unauthenticated)
}

func permissionCreateValidation(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	cred := authSuite(t, c)

	client := proto.NewPermissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(cred.AccessToken)),
	))

	req := proto.PermissionCreateRequest{
		Service:     "",
		Method:      "",
		Description: "",
	}

	_, err := client.Create(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)

	assertValidation(t, err, map[string]string{
		"service":     "cannot be blank",
		"method":      "cannot be blank",
		"description": "cannot be blank",
	})
}

func TestFindPermission(t *testing.T) {
	t.Run("ok", permissionFindOK)
	t.Run("unauthorized", permissionFindUnauthorized)
	t.Run("validation", permissionFindValidation)
	t.Run("wrong format", permissionFindWrongFormat)
	t.Run("not found", permissionFindNotFound)
}

func permissionFindOK(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, permissionID := permissionSuite(t, c)

	client := proto.NewPermissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.PermissionFindRequest{
		Id: permissionID,
	}

	resp, err := client.Find(context.Background(), &req)
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	assert.Equal(t, "proto.MyService", resp.Service)
	assert.Equal(t, "Find", resp.Method)
	assert.Equal(t, "Find a my service", resp.Description)
}

func permissionFindUnauthorized(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	_, permissionID := permissionSuite(t, c)

	client := proto.NewPermissionClient(c.connection(t))

	req := proto.PermissionFindRequest{
		Id: permissionID,
	}

	_, err := client.Find(context.Background(), &req)
	assertCode(t, err, codes.Unauthenticated)

}

func permissionFindValidation(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _ := permissionSuite(t, c)

	client := proto.NewPermissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.PermissionFindRequest{
		Id: "",
	}

	_, err := client.Find(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)

	assertValidation(t, err, map[string]string{
		"id": "cannot be blank",
	})
}

func permissionFindWrongFormat(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _ := permissionSuite(t, c)

	client := proto.NewPermissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.PermissionFindRequest{
		Id: "wrong format",
	}

	_, err := client.Find(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)

	assertValidation(t, err, map[string]string{
		"id": "id is invalid",
	})
}

func permissionFindNotFound(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _ := permissionSuite(t, c)

	client := proto.NewPermissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.PermissionFindRequest{
		Id: uuid.NewV4().String(),
	}

	_, err := client.Find(context.Background(), &req)
	assertCode(t, err, codes.NotFound)

}

func TestUpdatePermission(t *testing.T) {
	t.Run("ok", permissionUpdateOK)
	t.Run("unauthorized", permissionUpdateUnauthorized)
	t.Run("validation", permissionUpdateValidation)
	t.Run("wrong format", permissionUpdateWrongFormat)
	t.Run("not found", permissionUpdateNotFound)
}

func permissionUpdateOK(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, permissionID := permissionSuite(t, c)

	client := proto.NewPermissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.PermissionUpdateRequest{
		Id:          permissionID,
		Service:     "proto.MyService",
		Method:      "Update",
		Description: "Update a my service by id",
	}

	resp, err := client.Update(context.Background(), &req)
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	assert.Equal(t, "proto.MyService", resp.Service)
	assert.Equal(t, "Update", resp.Method)
	assert.Equal(t, "Update a my service by id", resp.Description)
}

func permissionUpdateUnauthorized(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	_, permissionID := permissionSuite(t, c)

	client := proto.NewPermissionClient(c.connection(t))

	req := proto.PermissionUpdateRequest{
		Id:          permissionID,
		Service:     "proto.MyService",
		Method:      "Update",
		Description: "Update a my service by id",
	}

	_, err := client.Update(context.Background(), &req)
	assertCode(t, err, codes.Unauthenticated)
}

func permissionUpdateValidation(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _ := permissionSuite(t, c)

	client := proto.NewPermissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.PermissionUpdateRequest{
		Id:          "",
		Service:     "",
		Method:      "",
		Description: "",
	}

	_, err := client.Update(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)

	assertValidation(t, err, map[string]string{
		"id":          "cannot be blank",
		"service":     "cannot be blank",
		"method":      "cannot be blank",
		"description": "cannot be blank",
	})
}

func permissionUpdateWrongFormat(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _ := permissionSuite(t, c)

	client := proto.NewPermissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.PermissionUpdateRequest{
		Id:          "wrong format",
		Service:     "",
		Method:      "",
		Description: "",
	}

	_, err := client.Update(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)

	assertValidation(t, err, map[string]string{
		"id":          "id is invalid",
		"service":     "cannot be blank",
		"method":      "cannot be blank",
		"description": "cannot be blank",
	})
}

func permissionUpdateNotFound(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _ := permissionSuite(t, c)

	client := proto.NewPermissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.PermissionUpdateRequest{
		Id:          uuid.NewV4().String(),
		Service:     "proto.MyService",
		Method:      "Update",
		Description: "Update a my service by id",
	}

	_, err := client.Update(context.Background(), &req)
	assertCode(t, err, codes.NotFound)
}

func TestPermissionList(t *testing.T) {
	t.Run("ok", permissionListOK)
	t.Run("unauthorized", permissionaListUnauthorized)
}

func permissionListOK(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _ := permissionSuite(t, c)

	client := proto.NewPermissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	resp, err := client.Permissions(context.Background(), &emptypb.Empty{})
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	assert.Equal(t, 18, len(resp.Permissions))
}

func permissionaListUnauthorized(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	permissionSuite(t, c)

	client := proto.NewPermissionClient(c.connection(t))

	_, err := client.Permissions(context.Background(), &emptypb.Empty{})
	assertCode(t, err, codes.Unauthenticated)
}

func TestDeletePermission(t *testing.T) {
	t.Run("ok", permissionDeleteOK)
	t.Run("unauthorized", permissionDeleteUnauthorized)
	t.Run("validation", permissionDeleteValidation)
	t.Run("wrong format", permissionDeleteWrongFormat)
	t.Run("not found", permissionDeleteNotFound)
}

func permissionDeleteOK(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, permissionID := permissionSuite(t, c)

	client := proto.NewPermissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.PermissionDeleteRequest{
		Id: permissionID,
	}

	_, err := client.Delete(context.Background(), &req)
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	result, err := client.Permissions(context.Background(), &emptypb.Empty{})
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	assert.Equal(t, 17, len(result.Permissions))
}

func permissionDeleteUnauthorized(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	_, permissionID := permissionSuite(t, c)

	client := proto.NewPermissionClient(c.connection(t))

	req := proto.PermissionDeleteRequest{
		Id: permissionID,
	}

	_, err := client.Delete(context.Background(), &req)
	assertCode(t, err, codes.Unauthenticated)
}

func permissionDeleteValidation(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _ := permissionSuite(t, c)

	client := proto.NewPermissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.PermissionDeleteRequest{
		Id: "",
	}

	_, err := client.Delete(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)

	assertValidation(t, err, map[string]string{
		"id": "cannot be blank",
	})
}

func permissionDeleteWrongFormat(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _ := permissionSuite(t, c)

	client := proto.NewPermissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.PermissionDeleteRequest{
		Id: "wrong format",
	}

	_, err := client.Delete(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)

	assertValidation(t, err, map[string]string{
		"id": "id is invalid",
	})
}

func permissionDeleteNotFound(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _ := permissionSuite(t, c)

	client := proto.NewPermissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.PermissionDeleteRequest{
		Id: uuid.NewV4().String(),
	}

	_, err := client.Delete(context.Background(), &req)
	assertCode(t, err, codes.NotFound)
}

func permissionSuite(t testing.TB, c *config) (string, string) {
	cred := authSuite(t, c)

	client := proto.NewPermissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(cred.AccessToken)),
	))

	req := proto.PermissionCreateRequest{
		Service:     "proto.MyService",
		Method:      "Find",
		Description: "Find a my service",
	}

	resp, err := client.Create(context.Background(), &req)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	return cred.AccessToken, resp.Id

}
