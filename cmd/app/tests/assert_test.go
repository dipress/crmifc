package tests

import (
	"testing"

	"gitlab.com/dipress/crmifc/internal/proto"
	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func assertCode(
	t testing.TB,
	err error,
	code codes.Code,
) {
	t.Helper()

	st, ok := status.FromError(err)
	if !ok {
		t.Errorf("unknown error: %v", err)
	}

	if st.Code() != code {
		t.Errorf("unexpected code: %d", st.Code())
	}
}



func assertTokens(
	t testing.TB,
	tokens *proto.TokensResponse,
) {
	t.Helper()

	if len(tokens.AccessToken) == 0 || len(tokens.RefreshToken) == 0 {
		t.Errorf("access or refresh tokens are blank")
	}
}


func assertValidation(
	t testing.TB,
	err error,
	expect map[string]string,
) {
	st, ok := status.FromError(err)
	if !ok {
		t.Errorf("unknown error: %v", err)
	}

	if st.Code() != codes.InvalidArgument {
		t.Errorf("unexpected error: %v", err)
	}

	got := make(map[string]string)
	st = status.Convert(err)
	for _, detail := range st.Details() {
		switch t := detail.(type) {
		case *errdetails.BadRequest_FieldViolation:
			got[t.GetField()] = t.GetDescription()
		}
	}

	if len(got) != len(expect) {
		t.Errorf("wrong details length")
	}

	for field, value := range expect {
		g, ok := got[field]
		if !ok {
			t.Errorf("details for field %s not exists", field)
		}

		if g != value {
			t.Errorf("details for field %s value %s expected %s", field, g, value)
		}
	}
}

