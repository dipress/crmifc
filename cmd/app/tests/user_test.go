package tests

import (
	"context"
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/dipress/crmifc/internal/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/emptypb"
)

func TestUserCreate(t *testing.T) {
	t.Run("ok", userCreateOK)
	t.Run("validation", userCreateValidation)
	t.Run("unauthorized", userCreateUnauthorized)
	t.Run("email already exists", userCreateEmailExists)
}

func userCreateOK(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, roleID := roleSuite(t, c)

	req := proto.UserCreateRequest{
		RoleId:    roleID,
		Email:     "jcolt@example.com",
		Password:  "Password123-",
		FirstName: "Johnny",
		LastName:  "Colt",
	}

	client := proto.NewUserClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	resp, err := client.Create(context.Background(), &req)
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	assert.Equal(t, "jcolt@example.com", resp.Email)
	assert.Equal(t, "Johnny", resp.FirstName)
	assert.Equal(t, "Colt", resp.LastName)
}

func userCreateValidation(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _ := roleSuite(t, c)

	req := proto.UserCreateRequest{
		RoleId:   "wrong format",
		Email:    "wrong email format",
		Password: "password",
	}

	client := proto.NewUserClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	_, err := client.Create(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)

	assertValidation(t, err, map[string]string{
		"role_id":  "id is invalid",
		"email":    "must be a valid email address",
		"password": "password is invalid",
	})
}

func userCreateUnauthorized(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	_, roleID := roleSuite(t, c)

	req := proto.UserCreateRequest{
		RoleId:   roleID,
		Email:    "jcolt@example.com",
		Password: "Password123-",
	}

	client := proto.NewUserClient(c.connection(t))

	_, err := client.Create(context.Background(), &req)
	assertCode(t, err, codes.Unauthenticated)
}

func userCreateEmailExists(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, roleID := roleSuite(t, c)

	req := proto.UserCreateRequest{
		RoleId:   roleID,
		Email:    "johnDo@example.com",
		Password: "Password123-",
	}

	client := proto.NewUserClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	_, err := client.Create(context.Background(), &req)
	assertCode(t, err, codes.AlreadyExists)
}

func TestUserFind(t *testing.T) {
	t.Run("ok", userFindOK)
	t.Run("validation", userFindValidation)
	t.Run("wrong format", userFindWrongFormat)
	t.Run("unauthorized", userFindUnauthorized)
	t.Run("not found", userFindNotFound)
}

func userFindOK(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _, userID := userSuite(t, c)

	req := proto.UserFindRequest{
		Id: userID,
	}

	client := proto.NewUserClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	resp, err := client.Find(context.Background(), &req)
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	assert.Equal(t, userID, resp.Id)
	assert.Equal(t, "johnyBee@example.com", resp.Email)
	assert.Equal(t, "Johny", resp.FirstName)
	assert.Equal(t, "Bee", resp.LastName)
}

func userFindValidation(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _, _ := userSuite(t, c)

	req := proto.UserFindRequest{
		Id: "",
	}

	client := proto.NewUserClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	_, err := client.Find(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)

	assertValidation(t, err, map[string]string{
		"id": "cannot be blank",
	})
}

func userFindWrongFormat(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _, _ := userSuite(t, c)

	req := proto.UserFindRequest{
		Id: "wrong format",
	}

	client := proto.NewUserClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	_, err := client.Find(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)

	assertValidation(t, err, map[string]string{
		"id": "id is invalid",
	})
}

func userFindUnauthorized(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	_, _, userID := userSuite(t, c)

	req := proto.UserFindRequest{
		Id: userID,
	}

	client := proto.NewUserClient(c.connection(t))

	_, err := client.Find(context.Background(), &req)
	assertCode(t, err, codes.Unauthenticated)
}

func userFindNotFound(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _, _ := userSuite(t, c)

	req := proto.UserFindRequest{
		Id: uuid.NewV4().String(),
	}

	client := proto.NewUserClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	_, err := client.Find(context.Background(), &req)
	assertCode(t, err, codes.NotFound)
}

func TestUserUpdate(t *testing.T) {
	t.Run("ok", userUpdateOK)
	t.Run("validation", userUpdateValidation)
	t.Run("wrong format", userUpdateWrongFormat)
	t.Run("unauthorized", userUpdateUnauthorized)
	t.Run("id not found", userUpdateIdNotFound)
	t.Run("role id not found", userUpdateRoleIdNotFound)
}

func userUpdateOK(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, roleID, userID := userSuite(t, c)

	req := proto.UserUpdateRequest{
		Id:        userID,
		RoleId:    roleID,
		Email:     "claus@example.com",
		FirstName: "Claus",
		LastName:  "Mo",
	}

	client := proto.NewUserClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	resp, err := client.Update(context.Background(), &req)
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	assert.Equal(t, userID, resp.Id)
	assert.Equal(t, roleID, resp.RoleId)
	assert.Equal(t, "claus@example.com", resp.Email)
	assert.Equal(t, "Claus", resp.FirstName)
	assert.Equal(t, "Mo", resp.LastName)
}

func userUpdateValidation(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _, _ := userSuite(t, c)

	req := proto.UserUpdateRequest{
		Id:        "",
		RoleId:    "",
		Email:     "",
		FirstName: "",
		LastName:  "",
	}

	client := proto.NewUserClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	_, err := client.Update(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)

	assertValidation(t, err, map[string]string{
		"id":      "cannot be blank",
		"role_id": "cannot be blank",
		"email":   "cannot be blank",
	})
}

func userUpdateWrongFormat(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _, _ := userSuite(t, c)

	req := proto.UserUpdateRequest{
		Id:        "wrong format",
		RoleId:    "wrong format",
		Email:     "wrong format",
		FirstName: "",
		LastName:  "",
	}

	client := proto.NewUserClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	_, err := client.Update(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)

	assertValidation(t, err, map[string]string{
		"id":      "id is invalid",
		"role_id": "id is invalid",
		"email":   "must be a valid email address",
	})
}

func userUpdateUnauthorized(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	_, roleID, userID := userSuite(t, c)

	req := proto.UserUpdateRequest{
		Id:        userID,
		RoleId:    roleID,
		Email:     "claus@example.com",
		FirstName: "Claus",
		LastName:  "Mo",
	}

	client := proto.NewUserClient(c.connection(t))

	_, err := client.Update(context.Background(), &req)
	assertCode(t, err, codes.Unauthenticated)
}

func userUpdateIdNotFound(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, roleID, _ := userSuite(t, c)

	req := proto.UserUpdateRequest{
		Id:        uuid.NewV4().String(),
		RoleId:    roleID,
		Email:     "claus@example.com",
		FirstName: "Claus",
		LastName:  "Mo",
	}

	client := proto.NewUserClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	_, err := client.Update(context.Background(), &req)
	assertCode(t, err, codes.NotFound)
}

func userUpdateRoleIdNotFound(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _, userID := userSuite(t, c)

	req := proto.UserUpdateRequest{
		Id:        userID,
		RoleId:    uuid.NewV4().String(),
		Email:     "claus@example.com",
		FirstName: "Claus",
		LastName:  "Mo",
	}

	client := proto.NewUserClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	_, err := client.Update(context.Background(), &req)
	assertCode(t, err, codes.NotFound)
}

func TestUserList(t *testing.T) {
	t.Run("ok", userListOK)
	t.Run("unauthorized", userListUnauthorized)
}

func userListOK(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _ := roleSuite(t, c)

	client := proto.NewUserClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	resp, err := client.Users(context.Background(), &emptypb.Empty{})
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	assert.Equal(t, 1, len(resp.Users))
}

func userListUnauthorized(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	roleSuite(t, c)

	client := proto.NewUserClient(c.connection(t))

	_, err := client.Users(context.Background(), &emptypb.Empty{})
	assertCode(t, err, codes.Unauthenticated)
}

func TestUserDelete(t *testing.T) {
	t.Run("ok", userDeleteOK)
	t.Run("unauthorized", userDeleteUnauthorized)
	t.Run("validation", userDeleteValidation)
	t.Run("wrong format", userDeleteWrongFormat)
	t.Run("not found", userDeleteNotFound)
}

func userDeleteOK(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _, userID := userSuite(t, c)

	client := proto.NewUserClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.UserDeleteRequest{
		Id: userID,
	}

	_, err := client.Delete(context.Background(), &req)
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	result, err := client.Users(context.Background(), &emptypb.Empty{})
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	assert.Equal(t, 1, len(result.Users))
}

func userDeleteUnauthorized(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	_, _, userID := userSuite(t, c)

	client := proto.NewUserClient(c.connection(t))

	req := proto.UserDeleteRequest{
		Id: userID,
	}

	_, err := client.Delete(context.Background(), &req)
	assertCode(t, err, codes.Unauthenticated)
}

func userDeleteValidation(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _, _ := userSuite(t, c)

	client := proto.NewUserClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.UserDeleteRequest{
		Id: "",
	}

	_, err := client.Delete(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)

	assertValidation(t, err, map[string]string{
		"id": "cannot be blank",
	})
}

func userDeleteWrongFormat(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _, _ := userSuite(t, c)

	client := proto.NewUserClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.UserDeleteRequest{
		Id: "wrong format",
	}

	_, err := client.Delete(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)

	assertValidation(t, err, map[string]string{
		"id": "id is invalid",
	})
}

func userDeleteNotFound(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _, _ := userSuite(t, c)

	client := proto.NewUserClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.UserDeleteRequest{
		Id: uuid.NewV4().String(),
	}

	_, err := client.Delete(context.Background(), &req)
	assertCode(t, err, codes.NotFound)
}

func userSuite(t testing.TB, c *config) (string, string, string) {
	token, roleID := roleSuite(t, c)

	client := proto.NewUserClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.UserCreateRequest{
		RoleId:    roleID,
		Email:     "johnyBee@example.com",
		Password:  "Password1234!",
		FirstName: "Johny",
		LastName:  "Bee",
	}

	user, err := client.Create(context.Background(), &req)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	return token, roleID, user.Id
}
