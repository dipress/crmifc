package tests

import (
	"context"
	"testing"
	"time"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/dipress/crmifc/internal/proto"
	"google.golang.org/grpc/codes"
)

func TestRefreshToken(t *testing.T) {
	t.Run("ok", refreshTokenOK)
	t.Run("validation", refreshTokenValidations)
	t.Run("expired", refreshTokenExpired)
}

func refreshTokenOK(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	tokens := authSuite(t, c)

	client := proto.NewAuthClient(c.connection(t))

	req := proto.RefreshTokenRequest{
		Token: tokens.RefreshToken,
	}

	tokens, err := client.RefreshToken(context.Background(), &req)
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	assertTokens(t, tokens)
}

func refreshTokenValidations(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	authSuite(t, c)

	client := proto.NewAuthClient(c.connection(t))
	_, err := client.RefreshToken(context.Background(), new(proto.RefreshTokenRequest))

	assertValidation(t, err, map[string]string{
		"token": "cannot be blank",
	})
}

func refreshTokenExpired(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	fourDaysAgo := time.Now().Add(-1 * (time.Hour * 24 * 4))

	tokens, err := c.authenticator.Tokens(
		context.Background(),
		uuid.NewV4().String(),
		fourDaysAgo,
	)

	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	client := proto.NewAuthClient(c.connection(t))

	req := proto.RefreshTokenRequest{
		Token: tokens.Refresh,
	}

	_, err = client.RefreshToken(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)
}
