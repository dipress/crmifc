package tests

import (
	"log"
	"net"
	"os"
	"testing"
	"time"

	"github.com/ory/dockertest"
	"gitlab.com/dipress/crmifc/internal/docker"
)

const (
	caseTimeout = 5 * time.Second
)

var (
	pool *dockertest.Pool

	pg *docker.Postgres
)

func TestMain(m *testing.M) {
	teardown := setupDocker()
	code := m.Run()
	teardown()
	os.Exit(code)
}

func setupDocker() func() {
	var err error
	pool, err = dockertest.NewPool("")
	if err != nil {
		log.Fatalf("create new pool: %v", err)
	}

	pd, err := docker.NewPostgres(pool, docker.SetConnTimeout(3*time.Minute))
	if err != nil {
		log.Fatalf("start postgres image: %v", err)
	}
	pg = pd

	return func() {
		pg.DB.Close()
		pool.Purge(pg.Resource) // nolint: errcheck
	}
}

func addr(lis net.Listener) string {
	return lis.Addr().String()
}

