package tests

import (
	"context"
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/dipress/crmifc/internal/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/emptypb"
)

func TestRoleCreate(t *testing.T) {
	t.Run("ok", roleCreateOK)
	t.Run("name already exists", roleCreateNameExists)
	t.Run("unauthorized", roleCreateUnauthorized)
	t.Run("validation", roleCreateValidation)
}

func roleCreateOK(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	cred := authSuite(t, c)

	client := proto.NewRoleClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(cred.AccessToken)),
	))

	req := proto.RoleCreateRequest{
		Name:        "Manager",
		Description: "Managers",
	}

	response, err := client.Create(context.Background(), &req)
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	assert.Equal(t, "Manager", response.Name)
	assert.Equal(t, "Managers", response.Description)
}

func roleCreateNameExists(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	cred := authSuite(t, c)

	client := proto.NewRoleClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(cred.AccessToken)),
	))

	req := proto.RoleCreateRequest{
		Name:        "Admin",
		Description: "Admins",
	}

	_, err := client.Create(context.Background(), &req)
	assertCode(t, err, codes.AlreadyExists)
}

func roleCreateUnauthorized(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	client := proto.NewRoleClient(c.connection(t))

	req := proto.RoleCreateRequest{
		Name:        "Manager",
		Description: "Managers",
	}

	_, err := client.Create(context.Background(), &req)
	assertCode(t, err, codes.Unauthenticated)
}

func roleCreateValidation(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	cred := authSuite(t, c)

	client := proto.NewRoleClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(cred.AccessToken)),
	))

	req := proto.RoleCreateRequest{
		Name:        "",
		Description: "",
	}

	_, err := client.Create(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)

	assertValidation(t, err, map[string]string{
		"name":        "cannot be blank",
		"description": "cannot be blank",
	})
}

func TestRoleFind(t *testing.T) {
	t.Run("ok", roleFindOK)
	t.Run("unauthorized", roleFindUnauthorized)
	t.Run("validation", roleFindValidation)
	t.Run("wrong format", roleFindWrongFormat)
	t.Run("not found", roleFindNotFound)
}

func roleFindOK(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, roleID := roleSuite(t, c)

	client := proto.NewRoleClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.RoleFindRequest{
		Id: roleID,
	}

	resp, err := client.Find(context.Background(), &req)
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	assert.Equal(t, roleID, resp.Id)
	assert.Equal(t, "Member", resp.Name)
	assert.Equal(t, "Members", resp.Description)
}

func roleFindUnauthorized(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	_, roleID := roleSuite(t, c)

	client := proto.NewRoleClient(c.connection(t))

	req := proto.RoleFindRequest{
		Id: roleID,
	}

	_, err := client.Find(context.Background(), &req)
	assertCode(t, err, codes.Unauthenticated)
}

func roleFindValidation(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _ := roleSuite(t, c)

	client := proto.NewRoleClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.RoleFindRequest{
		Id: "",
	}

	_, err := client.Find(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)

	assertValidation(t, err, map[string]string{
		"id": "cannot be blank",
	})
}

func roleFindWrongFormat(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _ := roleSuite(t, c)

	client := proto.NewRoleClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.RoleFindRequest{
		Id: "wrong format",
	}

	_, err := client.Find(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)

	assertValidation(t, err, map[string]string{
		"id": "id is invalid",
	})
}

func roleFindNotFound(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _ := roleSuite(t, c)

	client := proto.NewRoleClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.RoleFindRequest{
		Id: uuid.NewV4().String(),
	}

	_, err := client.Find(context.Background(), &req)
	assertCode(t, err, codes.NotFound)
}

func TestRoleUpdate(t *testing.T) {
	t.Run("ok", roleUpdateOK)
	t.Run("unauthorized", roleUpdateUnauthorized)
	t.Run("validation", roleUpdateValidation)
	t.Run("wrong format", roleUpdateWrongFormat)
	t.Run("not found", roleUpdateNotFound)
}

func roleUpdateOK(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, roleID := roleSuite(t, c)

	client := proto.NewRoleClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.RoleUpdateRequest{
		Id:          roleID,
		Name:        "Customer",
		Description: "Customers",
	}

	resp, err := client.Update(context.Background(), &req)
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	assert.Equal(t, roleID, resp.Id)
	assert.Equal(t, "Customer", resp.Name)
	assert.Equal(t, "Customers", resp.Description)
}

func roleUpdateUnauthorized(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	_, roleID := roleSuite(t, c)

	client := proto.NewRoleClient(c.connection(t))

	req := proto.RoleUpdateRequest{
		Id:          roleID,
		Name:        "Customer",
		Description: "Customers",
	}

	_, err := client.Update(context.Background(), &req)
	assertCode(t, err, codes.Unauthenticated)
}

func roleUpdateValidation(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _ := roleSuite(t, c)

	client := proto.NewRoleClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.RoleUpdateRequest{
		Id:          "",
		Name:        "",
		Description: "",
	}

	_, err := client.Update(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)

	assertValidation(t, err, map[string]string{
		"id":          "cannot be blank",
		"name":        "cannot be blank",
		"description": "cannot be blank",
	})
}

func roleUpdateWrongFormat(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _ := roleSuite(t, c)

	client := proto.NewRoleClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.RoleUpdateRequest{
		Id:          "wrong format",
		Name:        "",
		Description: "",
	}

	_, err := client.Update(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)

	assertValidation(t, err, map[string]string{
		"id":          "id is invalid",
		"name":        "cannot be blank",
		"description": "cannot be blank",
	})
}

func roleUpdateNotFound(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _ := roleSuite(t, c)

	client := proto.NewRoleClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.RoleUpdateRequest{
		Id:          uuid.NewV4().String(),
		Name:        "Customer",
		Description: "Customers",
	}

	_, err := client.Update(context.Background(), &req)
	assertCode(t, err, codes.NotFound)
}

func TestRoleList(t *testing.T) {
	t.Run("ok", roleListOK)
	t.Run("unauthorized", roleListUnauthorized)
}

func roleListOK(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _ := roleSuite(t, c)

	client := proto.NewRoleClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	resp, err := client.Roles(context.Background(), &emptypb.Empty{})
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	assert.Equal(t, 2, len(resp.Roles))
}

func roleListUnauthorized(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	roleSuite(t, c)

	client := proto.NewRoleClient(c.connection(t))

	_, err := client.Roles(context.Background(), &emptypb.Empty{})
	assertCode(t, err, codes.Unauthenticated)
}

func TestRoleDelete(t *testing.T) {
	t.Run("ok", roleDeleteOK)
	t.Run("unauthorized", roleDeleteUnauthorized)
	t.Run("validation", roleDeleteValidation)
	t.Run("wrong format", roleDeleteWrongFormat)
	t.Run("not found", roleDeleteNotFound)
}

func roleDeleteOK(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, roleID := roleSuite(t, c)

	client := proto.NewRoleClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.RoleDeleteRequest{
		Id: roleID,
	}

	_, err := client.Delete(context.Background(), &req)
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	result, err := client.Roles(context.Background(), &emptypb.Empty{})
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	assert.Equal(t, 1, len(result.Roles))
}

func roleDeleteUnauthorized(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	_, roleID := roleSuite(t, c)

	client := proto.NewRoleClient(c.connection(t))

	req := proto.RoleDeleteRequest{
		Id: roleID,
	}

	_, err := client.Delete(context.Background(), &req)
	assertCode(t, err, codes.Unauthenticated)
}

func roleDeleteValidation(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _ := roleSuite(t, c)

	client := proto.NewRoleClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.RoleDeleteRequest{
		Id: "",
	}

	_, err := client.Delete(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)

	assertValidation(t, err, map[string]string{
		"id": "cannot be blank",
	})
}

func roleDeleteWrongFormat(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _ := roleSuite(t, c)

	client := proto.NewRoleClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.RoleDeleteRequest{
		Id: "wrong format",
	}

	_, err := client.Delete(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)

	assertValidation(t, err, map[string]string{
		"id": "id is invalid",
	})
}

func roleDeleteNotFound(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _ := roleSuite(t, c)

	client := proto.NewRoleClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.RoleDeleteRequest{
		Id: uuid.NewV4().String(),
	}

	_, err := client.Delete(context.Background(), &req)
	assertCode(t, err, codes.NotFound)
}

func roleSuite(t testing.TB, c *config) (string, string) {
	cred := authSuite(t, c)

	client := proto.NewRoleClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(cred.AccessToken)),
	))

	req := proto.RoleCreateRequest{
		Name:        "Member",
		Description: "Members",
	}

	resp, err := client.Create(context.Background(), &req)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	return cred.AccessToken, resp.Id

}
