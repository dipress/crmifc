package tests

import (
	"context"
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/dipress/crmifc/internal/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/emptypb"
)

func TestCreateAdmission(t *testing.T) {
	t.Run("ok", admissionOK)
	t.Run("unauthorized", admissionCreateUnauthorized)
	t.Run("validation", admissionCreateValidation)
}

func admissionOK(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, roleID, permissionID := admissionSuite(t, c)

	client := proto.NewAdmissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.AdmissionAddRequest{
		RoleId:       roleID,
		PermissionId: permissionID,
	}

	resp, err := client.Add(context.Background(), &req)
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	assert.Equal(t, roleID, resp.RoleId)
	assert.Equal(t, permissionID, resp.PermissionId)
}

func admissionCreateUnauthorized(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	_, roleID, permissionID := admissionSuite(t, c)

	client := proto.NewAdmissionClient(c.connection(t))

	req := proto.AdmissionAddRequest{
		RoleId:       roleID,
		PermissionId: permissionID,
	}

	_, err := client.Add(context.Background(), &req)
	assertCode(t, err, codes.Unauthenticated)
}

func admissionCreateValidation(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _, _ := admissionSuite(t, c)

	client := proto.NewAdmissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.AdmissionAddRequest{
		RoleId:       "",
		PermissionId: "",
	}

	_, err := client.Add(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)

	assertValidation(t, err, map[string]string{
		"role_id":       "cannot be blank",
		"permission_id": "cannot be blank",
	})
}

func TestAdmissionList(t *testing.T) {
	t.Run("ok", admissionListOK)
	t.Run("unauthorized", admissionListUnauthorized)
}

func admissionListOK(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	cred := authSuite(t, c)

	client := proto.NewAdmissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(cred.AccessToken)),
	))

	resp, err := client.Admissions(context.Background(), &emptypb.Empty{})
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	assert.Equal(t, 17, len(resp.Admissions))
}

func admissionListUnauthorized(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	client := proto.NewAdmissionClient(c.connection(t))

	_, err := client.Admissions(context.Background(), &emptypb.Empty{})
	assertCode(t, err, codes.Unauthenticated)
}

func TestAdmissionRemove(t *testing.T) {
	t.Run("ok", admissionRemoveOK)
	t.Run("unauthorized", admissionRemoveUnauthorized)
	t.Run("validation", admissionRemoveValidation)
	t.Run("wrong format", admissionRemoveWrongFormat)
	t.Run("not found", admissionRemoveNotFound)
}

func admissionRemoveOK(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, roleID, permissionID := admissionCreateSuite(t, c)

	client := proto.NewAdmissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.AdmissionRemoveRequest{
		RoleId:       roleID,
		PermissionId: permissionID,
	}

	_, err := client.Remove(context.Background(), &req)
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	resp, err := client.Admissions(context.Background(), &emptypb.Empty{})
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	assert.Equal(t, 17, len(resp.Admissions))
}

func admissionRemoveUnauthorized(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	_, roleID, permissionID := admissionCreateSuite(t, c)

	client := proto.NewAdmissionClient(c.connection(t))

	req := proto.AdmissionRemoveRequest{
		RoleId:       roleID,
		PermissionId: permissionID,
	}

	_, err := client.Remove(context.Background(), &req)
	assertCode(t, err, codes.Unauthenticated)
}

func admissionRemoveValidation(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _, _ := admissionCreateSuite(t, c)

	client := proto.NewAdmissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.AdmissionRemoveRequest{
		RoleId:       "",
		PermissionId: "",
	}

	_, err := client.Remove(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)

	assertValidation(t, err, map[string]string{
		"role_id":       "cannot be blank",
		"permission_id": "cannot be blank",
	})
}

func admissionRemoveWrongFormat(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _, _ := admissionCreateSuite(t, c)

	client := proto.NewAdmissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.AdmissionRemoveRequest{
		RoleId:       "wrong format",
		PermissionId: "wrong format",
	}

	_, err := client.Remove(context.Background(), &req)
	assertCode(t, err, codes.InvalidArgument)

	assertValidation(t, err, map[string]string{
		"role_id":       "id is invalid",
		"permission_id": "id is invalid",
	})
}

func admissionRemoveNotFound(t *testing.T) {
	t.Parallel()

	c := prepare(t)
	t.Cleanup(c.cleanup)

	token, _, _ := admissionCreateSuite(t, c)

	client := proto.NewAdmissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.AdmissionRemoveRequest{
		RoleId:       uuid.NewV4().String(),
		PermissionId: uuid.NewV4().String(),
	}

	_, err := client.Remove(context.Background(), &req)
	assertCode(t, err, codes.NotFound)
}

func admissionCreateSuite(t testing.TB, c *config) (string, string, string) {
	token, roleID, permissionID := admissionSuite(t, c)

	client := proto.NewAdmissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(token)),
	))

	req := proto.AdmissionAddRequest{
		RoleId:       roleID,
		PermissionId: permissionID,
	}

	resp, err := client.Add(context.Background(), &req)
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	return token, resp.RoleId, resp.PermissionId
}

func admissionSuite(t testing.TB, c *config) (string, string, string) {
	cred := authSuite(t, c)

	// create role
	roleClient := proto.NewRoleClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(cred.AccessToken)),
	))

	roleReq := proto.RoleCreateRequest{
		Name:        "Support",
		Description: "Support",
	}

	role, err := roleClient.Create(context.Background(), &roleReq)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	permissionClient := proto.NewPermissionClient(c.connection(
		t,
		grpc.WithPerRPCCredentials(tokenAuth(cred.AccessToken)),
	))

	permissionReq := proto.PermissionCreateRequest{
		Service:     "proto.newService",
		Method:      "Find",
		Description: "Find a new service",
	}

	permission, err := permissionClient.Create(context.Background(), &permissionReq)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	return cred.AccessToken, role.Id, permission.Id
}
