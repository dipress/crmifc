package main

import (
	"fmt"
	"net"
	"os"
	"os/signal"
	"syscall"

	"github.com/kelseyhightower/envconfig"
	"github.com/romanyx/stack"
	"gitlab.com/dipress/crmifc/cmd/app/runner"
	grpcAdapter "gitlab.com/dipress/crmifc/internal/adapter/grpc"
	ini "gitlab.com/dipress/crmifc/internal/init"
)

type spec struct {
	Addr string `default:":3000"`
}

func main() {
	var s spec
	if err := envconfig.Process("app", &s); err != nil {
		panic(stack.Errorf("parse env: %w", err))
	}

	c := ini.Infrastructure(
		ini.WithDB,
		ini.WithAuth,
		ini.WithLogger,
	)

	defer c.Teardown()

	var servers []runner.Server

	osSignals := make(chan os.Signal, 1)
	signal.Notify(osSignals, os.Interrupt, syscall.SIGTERM)

	lis, err := net.Listen("tcp", s.Addr)
	if err != nil {
		panic(fmt.Errorf("failed to listen: %w", err))
	}

	server := grpcAdapter.NewServer(
		lis,
		c.DB(),
		c.Auth(),
		c.Logger(),
	)

	servers = append(servers, server)

	runner.Run(osSignals, c.Logger(), servers...)
}
