LOCAL_BIN:=$(CURDIR)/bin
bindata_cmd =	go-bindata -prefix migrations/ -pkg schema -o migrations.bindata.go migrations/

proto-api:
	protoc \
	--proto_path=proto \
	--go_out=internal/proto \
	--go_opt=paths=source_relative \
	--plugin=protoc-gen-go=bin/protoc-gen-go \
	--go-grpc_out=internal/proto \
	--go-grpc_opt=paths=source_relative \
	--plugin=protoc-gen-go-grpc=bin/protoc-gen-go-grpc \
	proto/auth.proto \
	proto/role.proto \
	proto/permission.proto \
	proto/user.proto \
	proto/admission.proto 


install-deps:
	GOBIN=$(LOCAL_BIN) go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.28
	GOBIN=$(LOCAL_BIN) go install -mod=mod google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.2

get-deps:
	go get -u google.golang.org/protobuf/cmd/protoc-gen-go
	go get -u google.golang.org/grpc/cmd/protoc-gen-go-grpc

migrations:
	cd ./internal/storage/postgres/schema && $(bindata_cmd)

lint:
	golangci-lint run ./...
