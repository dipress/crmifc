The following tasks need to be accomplished:

Auth part:
[x] need to return jwt tokens pair.
[ ] need to change claim's subject to user's email.

User part:
[ ] create user with default role.
[ ] create a profile after the user was created.
[ ] update user's role by user id.
[ ] after delete user need to delete a profile and assosiated role.

Profile part:
[x] - update profile by user id.

Role part:
[x] - create CRUD for roles.
[ ] - add permission to role
[ ] - delete permission to role
[ ] - show all permissions by role

Permissions part:
[ ] - create CRUD for permissions

Users and Roles part:
[ ] - show user roles by user id.
[ ] - show all users by role id.
