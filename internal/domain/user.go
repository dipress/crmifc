package domain

import (
	"context"
	"errors"
	"time"

	"github.com/romanyx/stack"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/dipress/crmifc/internal/storage"
)

var (
	ErrEmailAlreadyExists = errors.New("email already exists")
	ErrUserNotFound       = errors.New("id not found")
	ErrRoleNotFound       = errors.New("role_id not found")
)

type userDAO interface {
	Store(context.Context, *storage.User) error
	Unique(context.Context, string) error
	ByEmail(context.Context, string) (*storage.User, error)
	ByID(context.Context, string) (*storage.User, error)
	Update(context.Context, *storage.User) error
	Delete(context.Context, string) error
	Users(context.Context) ([]storage.User, error)
}

// User contains user domain logic.
type User struct {
	user          userDAO
	role          roleDAO
	authenticator authenticator
}

// NewUser returns ready to use service.
func NewUser(
	ud userDAO,
	r roleDAO,
	auth authenticator,
) *User {
	u := User{
		user:          ud,
		role:          r,
		authenticator: auth,
	}

	return &u
}

type NewUserDTO struct {
	Role_ID  string
	Email    string
	Password string
	FistName string
	LastName string
}

// Create creates a new user.
func (s *User) Create(
	ctx context.Context,
	nu *NewUserDTO,
) (*UserDTO, error) {

	err := s.user.Unique(ctx, nu.Email)
	if err != nil && !errors.Is(err, storage.ErrEmailAlreadyExists) {
		return nil, stack.Errorf("unique: %w", err)
	}

	if errors.Is(err, storage.ErrEmailAlreadyExists) {
		return nil, ErrEmailAlreadyExists
	}

	pw, err := s.authenticator.PasswordHash(nu.Password)
	if err != nil {
		return nil, stack.Errorf("password hash: %w", err)
	}

	var u storage.User
	u.ID = uuid.NewV4().String()
	u.RoleID = nu.Role_ID
	u.Email = nu.Email
	u.PasswordHash = pw
	u.FirstName = nu.FistName
	u.LastName = nu.LastName

	if err := s.user.Store(ctx, &u); err != nil {
		return nil, stack.Errorf("user store: %w", err)
	}

	user, err := s.user.ByID(ctx, u.ID)
	if err != nil {
		return nil, stack.Errorf("user by id: %w", err)
	}

	return userResponseFromModel(user), nil
}

type UserDTO struct {
	ID        string
	Role_ID   string
	Email     string
	FirstName string
	LastName  string
	Created   time.Time
	Updated   time.Time
}

// Find finds user by id.
func (s *User) Find(
	ctx context.Context,
	id string,
) (*UserDTO, error) {
	user, err := s.user.ByID(ctx, id)
	if err != nil {
		return nil, stack.Errorf("find by id: %w", err)
	}

	return userResponseFromModel(user), nil
}

type UpdateUserDTO struct {
	ID        string
	Role_ID   string
	Email     string
	FirstName string
	LastName  string
}

// Update updates user by id.
func (s *User) Update(
	ctx context.Context,
	uu *UpdateUserDTO,
) (*UserDTO, error) {
	u, err := s.user.ByID(ctx, uu.ID)
	if err != nil {
		if errors.Is(err, storage.ErrNotFound) {
			return nil, ErrUserNotFound
		}

		return nil, stack.Errorf("user by id: %w", err)
	}

	r, err := s.role.ByID(ctx, uu.Role_ID)
	if err != nil {
		if errors.Is(err, storage.ErrNotFound) {
			return nil, ErrRoleNotFound
		}

		return nil, stack.Errorf("role by id: %w", err)
	}

	u.RoleID = r.ID
	u.Email = uu.Email
	u.FirstName = uu.FirstName
	u.LastName = uu.LastName

	if err := s.user.Update(ctx, u); err != nil {
		return nil, stack.Errorf("user update: %w", err)
	}

	user, err := s.user.ByID(ctx, u.ID)
	if err != nil {
		return nil, stack.Errorf("user by id: %w", err)
	}

	return userResponseFromModel(user), nil
}

// Delete deletes user by id.
func (s *User) Delete(
	ctx context.Context,
	id string,
) error {
	u, err := s.user.ByID(ctx, id)
	if err != nil {
		return stack.Errorf("find user by id: %w", err)
	}

	if err := s.user.Delete(ctx, u.ID); err != nil {
		return stack.Errorf("user delete by id: %w", err)
	}

	return nil
}

// Users returns all users.
func (s *User) Users(
	ctx context.Context,
) ([]UserDTO, error) {
	users, err := s.user.Users(ctx)
	if err != nil {
		return nil, stack.Errorf("users: %w", err)
	}

	var usr = make([]UserDTO, 0, len(users))
	for _, user := range users {
		usr = append(usr, *userResponseFromModel(&user))
	}

	return usr, nil
}

func userResponseFromModel(u *storage.User) *UserDTO {
	resp := UserDTO{
		ID:        u.ID,
		Role_ID:   u.RoleID,
		Email:     u.Email,
		FirstName: u.FirstName,
		LastName:  u.LastName,
		Created:   u.Timestamps.Created,
		Updated:   u.Timestamps.Updated,
	}

	return &resp
}
