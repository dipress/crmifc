package domain

import (
	"context"
	"errors"
	"time"

	"github.com/romanyx/stack"
	"gitlab.com/dipress/crmifc/internal/auth"
	"gitlab.com/dipress/crmifc/internal/storage"
)

var (
	// ErrEmailOrPasswordWrong returns when either email
	// or password passed for login is wrong.
	ErrEmailOrPasswordWrong = errors.New("login or password is wrong")
)

type authenticator interface {
	Tokens(ctx context.Context, id string, creates time.Time) (*auth.Tokens, error)
	ParseClaims(ctx context.Context, token string) (auth.Claims, error)
	PasswordHash(string) (string, error)
}

type Auth struct {
	users         userDAO
	authenticator authenticator
}

// NewAuth returns ready to use authentication
// logic.
func NewAuth(u userDAO, auth authenticator) *Auth {
	a := Auth{
		users:         u,
		authenticator: auth,
	}

	return &a
}

type CredentialDTO struct {
	Access  string
	Refresh string
}

// Login allows to login user using
// email password pair.
func (s *Auth) Login(
	ctx context.Context,
	email, password string,
) (*CredentialDTO, error) {
	user, err := s.users.ByEmail(ctx, email)
	if err != nil && !errors.Is(err, storage.ErrNotFound) {
		return nil, stack.Errorf("by email: %w", err)
	}

	if errors.Is(err, storage.ErrNotFound) {
		return nil, ErrEmailOrPasswordWrong
	}

	pw, err := s.authenticator.PasswordHash(password)
	if err != nil {
		return nil, stack.Errorf("password hash: %w", err)
	}

	if user.PasswordHash != string(pw) {
		return nil, ErrEmailOrPasswordWrong
	}

	tokens, err := s.authenticator.Tokens(ctx, user.ID, time.Now())
	if err != nil {
		return nil, stack.Errorf("tokens: %w", err)
	}

	return credentialResponse(tokens), nil
}

// Refresh allows to refresh tokens
// using refresh token.
func (s *Auth) Refresh(
	ctx context.Context,
	token string,
) (*CredentialDTO, error) {
	claims, err := s.authenticator.ParseClaims(ctx, token)
	if err != nil {
		return nil, stack.Errorf("parse claims: %w", err)
	}

	user, err := s.users.ByID(ctx, claims.StandardClaims.Subject)
	if err != nil && !errors.Is(err, storage.ErrNotFound) {
		return nil, stack.Errorf("by id: %w", err)
	}

	tokens, err := s.authenticator.Tokens(ctx, user.ID, time.Now())
	if err != nil {
		return nil, stack.Errorf("tokens: %w", err)
	}

	return credentialResponse(tokens), nil
}

func credentialResponse(
	t *auth.Tokens,
) *CredentialDTO {
	resp := CredentialDTO{
		Access:  t.Access,
		Refresh: t.Refresh,
	}

	return &resp
}
