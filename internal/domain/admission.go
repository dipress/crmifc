package domain

import (
	"context"

	"github.com/romanyx/stack"
	"gitlab.com/dipress/crmifc/internal/storage"
)

type admissionDAO interface {
	Store(context.Context, *storage.Admission) error
	Find(context.Context, *storage.Admission) (*storage.Admission, error)
	Delete(context.Context, *storage.Admission) error
	Admissions(context.Context) ([]storage.Admissions, error)
}

// Admission contains admission domain logic.
type Admission struct {
	admission admissionDAO
}

// NewAdmission returns ready to use service.
func NewAdmission(ad admissionDAO) *Admission {
	a := Admission{
		admission: ad,
	}

	return &a
}

type NewAdmissionDTO struct {
	RoleID       string
	PermissionID string
}

// Create creates a new admission.
func (s *Admission) Create(
	ctx context.Context,
	na *NewAdmissionDTO,
) (*AdmissionDTO, error) {
	var a storage.Admission
	a.RoleID = na.RoleID
	a.PermissionID = na.PermissionID

	if err := s.admission.Store(ctx, &a); err != nil {
		return nil, stack.Errorf("store: %w", err)
	}

	admission, err := s.admission.Find(ctx, &a)
	if err != nil {
		return nil, stack.Errorf("find: %w", err)
	}

	return admissionResponseFromModel(admission), nil
}

type DeleteAdmissionDTO struct {
	RoleID       string
	PermissionID string
}

// Delete deletes the admission.
func (s *Admission) Delete(
	ctx context.Context,
	da *DeleteAdmissionDTO,
) error {
	var a storage.Admission
	a.RoleID = da.RoleID
	a.PermissionID = da.PermissionID
	admision, err := s.admission.Find(ctx, &a)
	if err != nil {
		return stack.Errorf("find: %w", err)
	}

	if err := s.admission.Delete(ctx, admision); err != nil {
		return stack.Errorf("delete: %w", err)
	}

	return nil
}

type AdmissionsDTO struct {
	RoleID       string
	PermissionID string
	Name         string
	Service      string
	Method       string
	Description  string
}

// Admissions returns all admissions.
func (s *Admission) Admissions(
	ctx context.Context,
) ([]AdmissionsDTO, error) {
	admissions, err := s.admission.Admissions(ctx)
	if err != nil {
		return nil, stack.Errorf("admissions: %w", err)
	}

	var adms = make([]AdmissionsDTO, 0, len(admissions))
	for _, adm := range admissions {
		adms = append(adms, *admissionsResponseFromModel(&adm))
	}

	return adms, nil
}

type AdmissionDTO struct {
	RoleID       string
	PermissionID string
}

func admissionResponseFromModel(a *storage.Admission) *AdmissionDTO {
	resp := AdmissionDTO{
		RoleID:       a.RoleID,
		PermissionID: a.PermissionID,
	}

	return &resp
}

func admissionsResponseFromModel(a *storage.Admissions) *AdmissionsDTO {
	resp := AdmissionsDTO{
		RoleID:       a.RoleID,
		PermissionID: a.PermissionID,
		Name:         a.Name,
		Service:      a.Service,
		Method:       a.Method,
		Description:  a.Description,
	}

	return &resp
}
