package domain

import (
	"context"
	"time"

	"github.com/romanyx/stack"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/dipress/crmifc/internal/storage"
)

type permissionDAO interface {
	Store(context.Context, *storage.Permission) error
	ByID(context.Context, string) (*storage.Permission, error)
	Update(context.Context, *storage.Permission) error
	Delete(context.Context, string) error
	Permissions(context.Context) ([]storage.Permission, error)
}

// Permission contains permission domain logic.
type Permission struct {
	permission permissionDAO
}

// NewPermission returns ready to use service.
func NewPermission(pd permissionDAO) *Permission {
	p := Permission{
		permission: pd,
	}

	return &p
}

type NewPermissionDTO struct {
	Service     string
	Method      string
	Description string
}

// Create creates a new permission.
func (s *Permission) Create(
	ctx context.Context,
	np *NewPermissionDTO,
) (*PermissionDTO, error) {
	var p storage.Permission
	p.ID = uuid.NewV4().String()
	p.Service = np.Service
	p.Method = np.Method
	p.Description = np.Description

	if err := s.permission.Store(ctx, &p); err != nil {
		return nil, stack.Errorf("store: %w", err)
	}

	permission, err := s.permission.ByID(ctx, p.ID)
	if err != nil {
		return nil, stack.Errorf("by id: %w", err)
	}

	return permissionResponseFromModel(permission), nil
}

// Find finds permission by id.
func (s *Permission) Find(
	ctx context.Context,
	id string,
) (*PermissionDTO, error) {
	permission, err := s.permission.ByID(ctx, id)
	if err != nil {
		return nil, stack.Errorf("find: %w", err)
	}

	return permissionResponseFromModel(permission), nil
}

type UpdatePermissionDTO struct {
	ID          string
	Service     string
	Method      string
	Description string
}

// Update updates permission by id.
func (s *Permission) Update(
	ctx context.Context,
	up *UpdatePermissionDTO,
) (*PermissionDTO, error) {
	p, err := s.permission.ByID(ctx, up.ID)
	if err != nil {
		return nil, stack.Errorf("by id: %w", err)
	}

	p.Service = up.Service
	p.Method = up.Method
	p.Description = up.Description

	if err := s.permission.Update(ctx, p); err != nil {
		return nil, stack.Errorf("update: %w", err)
	}

	permission, err := s.permission.ByID(ctx, up.ID)
	if err != nil {
		return nil, stack.Errorf("by id: %w", err)
	}

	return permissionResponseFromModel(permission), nil
}

// Delete deletes permission by id.
func (s *Permission) Delete(
	ctx context.Context,
	id string,
) error {
	p, err := s.permission.ByID(ctx, id)
	if err != nil {
		return stack.Errorf("by id: %w", err)
	}

	if err := s.permission.Delete(ctx, p.ID); err != nil {
		return stack.Errorf("delete: %w", err)
	}

	return nil
}

// Permissions returns all permissions.
func (s *Permission) Permissions(
	ctx context.Context,
) ([]PermissionDTO, error) {
	permissions, err := s.permission.Permissions(ctx)
	if err != nil {
		return nil, stack.Errorf("permissions: %w", err)
	}

	var pls = make([]PermissionDTO, 0, len(permissions))
	for _, permission := range permissions {
		pls = append(pls, *permissionResponseFromModel(&permission))
	}

	return pls, nil
}

type PermissionDTO struct {
	ID          string
	Service     string
	Method      string
	Description string
	Created     time.Time
	Updated     time.Time
}

func permissionResponseFromModel(p *storage.Permission) *PermissionDTO {
	resp := PermissionDTO{
		ID:          p.ID,
		Service:     p.Service,
		Method:      p.Method,
		Description: p.Description,
		Created:     p.Timestamps.Created,
		Updated:     p.Timestamps.Updated,
	}

	return &resp
}
