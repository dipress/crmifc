package domain

import (
	"context"
	"errors"
	"time"

	"github.com/romanyx/stack"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/dipress/crmifc/internal/storage"
)

var (
	ErrNameAlreadyExists = errors.New("name already exists")
)

type roleDAO interface {
	Store(context.Context, *storage.Role) error
	Unique(context.Context, string) error
	ByID(context.Context, string) (*storage.Role, error)
	Roles(context.Context) ([]storage.Role, error)
	Update(context.Context, *storage.Role) error
	Delete(context.Context, string) error
}

// Role contains role domain logic.
type Role struct {
	role roleDAO
}

// NewRole returns ready to use service.
func NewRole(
	rd roleDAO,
) *Role {
	r := Role{
		role: rd,
	}

	return &r
}

type NewRoleDTO struct {
	Name        string
	Description string
}

// Create creates a new role.
func (s *Role) Create(
	ctx context.Context,
	nr *NewRoleDTO,
) (*RoleDTO, error) {
	err := s.role.Unique(ctx, nr.Name)
	if err != nil && !errors.Is(err, storage.ErrNameAlreadyExists) {
		return nil, stack.Errorf("unique: %w", err)
	}

	if errors.Is(err, storage.ErrNameAlreadyExists) {
		return nil, ErrNameAlreadyExists
	}

	var r storage.Role
	r.ID = uuid.NewV4().String()
	r.Name = nr.Name
	r.Description = nr.Description

	if err := s.role.Store(ctx, &r); err != nil {
		return nil, stack.Errorf("store: %w", err)
	}

	role, err := s.role.ByID(ctx, r.ID)
	if err != nil {
		return nil, stack.Errorf("by id: %w", err)
	}

	return roleResponseFromModel(role), nil
}

type RoleDTO struct {
	ID          string
	Name        string
	Description string
	Created     time.Time
	Updated     time.Time
}

// Find finds role by id.
func (s *Role) Find(
	ctx context.Context,
	id string,
) (*RoleDTO, error) {
	role, err := s.role.ByID(ctx, id)
	if err != nil {
		return nil, stack.Errorf("find: %w", err)
	}

	return roleResponseFromModel(role), nil
}

type UpdateRoleDTO struct {
	ID          string
	Name        string
	Description string
}

// Update updates role by id.
func (s *Role) Update(
	ctx context.Context,
	ur *UpdateRoleDTO,
) (*RoleDTO, error) {
	r, err := s.role.ByID(ctx, ur.ID)
	if err != nil {
		return nil, stack.Errorf("by id: %w", err)
	}

	r.Name = ur.Name
	r.Description = ur.Description

	if err := s.role.Update(ctx, r); err != nil {
		return nil, stack.Errorf("update: %w", err)
	}

	role, err := s.role.ByID(ctx, ur.ID)
	if err != nil {
		return nil, stack.Errorf("by id: %w", err)
	}

	return roleResponseFromModel(role), nil
}

// Delete deletes role by id.
func (s *Role) Delete(
	ctx context.Context,
	id string,
) error {
	r, err := s.role.ByID(ctx, id)
	if err != nil {
		return stack.Errorf("by id: %w", err)
	}

	if err := s.role.Delete(ctx, r.ID); err != nil {
		return stack.Errorf("delete: %w", err)
	}

	return nil
}

// Roles returns all roles.
func (s *Role) Roles(
	ctx context.Context,
) ([]RoleDTO, error) {
	roles, err := s.role.Roles(ctx)
	if err != nil {
		return nil, stack.Errorf("roles: %w", err)
	}

	var rls = make([]RoleDTO, 0, len(roles))
	for _, role := range roles {
		rls = append(rls, *roleResponseFromModel(&role))
	}

	return rls, nil
}

func roleResponseFromModel(r *storage.Role) *RoleDTO {
	resp := RoleDTO{
		ID:          r.ID,
		Name:        r.Name,
		Description: r.Description,
		Created:     r.Timestamps.Created,
		Updated:     r.Timestamps.Updated,
	}

	return &resp
}
