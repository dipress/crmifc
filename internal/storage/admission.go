package storage

type Admission struct {
	RoleID       string
	PermissionID string
}

type Admissions struct {
	RoleID       string
	PermissionID string
	Name         string
	Service      string
	Method       string
	Description  string
}
