package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	sq "github.com/Masterminds/squirrel"
	"github.com/romanyx/guard"
	"github.com/romanyx/stack"
	"gitlab.com/dipress/crmifc/internal/storage"
)

var (
	admissionColumns  = []string{"role_id", "permission_id"}
	admissionsColumns = []string{"roles.id", "roles.name", "permissions.id",
		"permissions.service", "permissions.method", "permissions.description"}
)

// Admission data access object.
type Admission struct {
	db *sql.DB
}

// NewAdmission returns ready to use admission's repository.
func NewAdmission(db *sql.DB) *Admission {
	guardNewAdmission(db)

	a := Admission{
		db: db,
	}

	return &a
}

// guardNewAdmission allows to guard NewAdmission constructor.
func guardNewAdmission(db *sql.DB) {
	guard.MustNotNil(1, "db", db)
}

// Store stores a new admission into database.
func (r *Admission) Store(
	ctx context.Context,
	model *storage.Admission,
) error {
	sql, args, err := psql.Insert("admissions").
		Columns(admissionColumns...).
		Values(
			model.RoleID,
			model.PermissionID,
		).
		ToSql()

	if err != nil {
		return stack.Errorf("build query: %w", err)
	}

	if _, err := r.db.ExecContext(ctx, sql, args...); err != nil {
		return stack.Errorf("exec context: %w", err)
	}

	return nil
}

// Find finds the admission.
func (r *Admission) Find(
	ctx context.Context,
	model *storage.Admission,
) (*storage.Admission, error) {
	query, args, err := psql.Select(admissionColumns...).
		From("admissions").
		Where(sq.And{
			sq.Eq{"role_id": model.RoleID},
			sq.Eq{"permission_id": model.PermissionID},
		}).
		ToSql()
	if err != nil {
		return nil, stack.Errorf("build query: %w", err)
	}

	var adm storage.Admission
	row := r.db.QueryRowContext(ctx, query, args...)
	if err := scanAdmission(row.Scan, &adm); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, r.notFound(model.RoleID, model.PermissionID)
		}

		return nil, stack.Errorf("scan query: %w", err)
	}

	return &adm, nil
}

// Delete deletes the admission into database.
func (r *Admission) Delete(
	ctx context.Context,
	model *storage.Admission,
) error {
	query, args, err := psql.Delete("admissions").
		Where(sq.And{
			sq.Eq{"role_id": model.RoleID},
			sq.Eq{"permission_id": model.PermissionID},
		}).
		ToSql()

	if err != nil {
		return stack.Errorf("build query: %w", err)
	}

	if _, err := r.db.ExecContext(ctx, query, args...); err != nil {
		return stack.Errorf("exec context: %w", err)
	}
	return nil
}

// Admissions returns all admissions.
func (r *Admission) Admissions(
	ctx context.Context,
) ([]storage.Admissions, error) {
	query, args, err := psql.Select(admissionsColumns...).
		From("admissions").
		InnerJoin("roles on admissions.role_id = roles.id").
		InnerJoin("permissions on admissions.permission_id = permissions.id").
		ToSql()

	if err != nil {
		return nil, stack.Errorf("build query: %w", err)
	}

	rows, err := r.db.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, stack.Errorf("query context: %w", err)
	}
	defer rows.Close()

	var admissions []storage.Admissions
	for rows.Next() {
		var ad storage.Admissions
		if err := scanAdmissions(rows.Scan, &ad); err != nil {
			return nil, stack.Errorf("scan query: %w", err)
		}

		admissions = append(admissions, ad)
	}
	return admissions, nil
}

func (r *Admission) notFound(roleID, permissionID string) error {
	return storage.NewNotFoundError(
		"admission",
		fmt.Sprintf("role_id: %s, permission_id: %s", roleID, permissionID),
	)
}

func scanAdmission(
	scanFunc func(dest ...interface{}) error,
	a *storage.Admission,
) error {
	return scanFunc(
		&a.RoleID,
		&a.PermissionID,
	)
}

func scanAdmissions(
	scanFunc func(dest ...interface{}) error,
	a *storage.Admissions,
) error {
	return scanFunc(
		&a.RoleID,
		&a.Name,
		&a.PermissionID,
		&a.Service,
		&a.Method,
		&a.Description,
	)
}
