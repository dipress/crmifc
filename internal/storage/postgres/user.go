package postgres

import (
	"context"
	"database/sql"
	"errors"

	sq "github.com/Masterminds/squirrel"
	"github.com/romanyx/guard"
	"github.com/romanyx/stack"
	"gitlab.com/dipress/crmifc/internal/storage"
)

var (
	userColumns = []string{"id", "role_id", "email", "password_hash",
		"first_name", "last_name",
		// those one are inserted by db.
		"created_at", "updated_at"}

	userInsertColumns = userColumns[:6]
)

// User data access object.
type User struct {
	db *sql.DB
}

// NewUser returns ready to use user's repository.
func NewUser(db *sql.DB) *User {
	guardNewUser(db)

	u := User{
		db: db,
	}

	return &u
}

// guardNewUser allows to guard NewUser constructor.
func guardNewUser(db *sql.DB) {
	guard.MustNotNil(1, "db", db)
}

// Store stores a new user into database.
func (r *User) Store(
	ctx context.Context,
	model *storage.User,
) error {
	sql, args, err := psql.Insert("users").
		Columns(userInsertColumns...).
		Values(
			model.ID,
			model.RoleID,
			model.Email,
			model.PasswordHash,
			model.FirstName,
			model.LastName,
		).
		ToSql()

	if err != nil {
		return stack.Errorf("build query: %w", err)
	}

	if _, err := r.db.ExecContext(ctx, sql, args...); err != nil {
		return stack.Errorf("exec context: %w", err)
	}

	return nil
}

func (r *User) Unique(
	ctx context.Context,
	email string,
) error {
	query, args, err := psql.Select("count(*)").
		From("users").
		Where(sq.Eq{"email": email}).
		ToSql()
	if err != nil {
		return stack.Errorf("build query: %w", err)
	}

	var c int
	if err := r.db.QueryRowContext(ctx, query, args...).Scan(&c); err != nil {
		return stack.Errorf("scan: %w", err)
	}

	if c > 0 {
		return storage.ErrEmailAlreadyExists
	}

	return nil
}

// ByEmail returns user by email.
func (r *User) ByEmail(
	ctx context.Context,
	email string,
) (*storage.User, error) {
	query, args, err := psql.Select(userColumns...).
		From("users").
		Where(sq.Eq{"email": email}).
		ToSql()
	if err != nil {
		return nil, stack.Errorf("build query: %w", err)
	}

	var u storage.User
	row := r.db.QueryRowContext(ctx, query, args...)
	if err := scanUser(row.Scan, &u); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, r.notFound(email)
		}

		return nil, stack.Errorf("scan query: %w", err)
	}

	return &u, nil
}

// ByID returns user by id.
func (r *User) ByID(
	ctx context.Context,
	id string,
) (*storage.User, error) {
	query, args, err := psql.Select(userColumns...).
		From("users").
		Where(sq.Eq{"id": id}).
		ToSql()
	if err != nil {
		return nil, stack.Errorf("build query: %w", err)
	}

	var u storage.User
	row := r.db.QueryRowContext(ctx, query, args...)
	if err := scanUser(row.Scan, &u); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, r.notFound(id)
		}

		return nil, stack.Errorf("scan query: %w", err)
	}

	return &u, nil
}

// Update updates user by id.
func (r *User) Update(
	ctx context.Context,
	model *storage.User,
) error {
	query, args, err := psql.Update("users").
		SetMap(map[string]interface{}{
			"role_id":    model.RoleID,
			"email":      model.Email,
			"first_name": model.FirstName,
			"last_name":  model.LastName,
		}).
		Where(sq.Eq{"id": model.ID}).
		ToSql()

	if err != nil {
		return stack.Errorf("build query: %w", err)
	}

	if _, err := r.db.ExecContext(ctx, query, args...); err != nil {
		return stack.Errorf("exec context: %w", err)
	}

	return nil
}

// Delete deletes user by id
func (r *User) Delete(
	ctx context.Context,
	id string,
) error {
	query, args, err := psql.Delete("users").
		Where(sq.Eq{"id": id}).
		ToSql()

	if err != nil {
		return stack.Errorf("build query: %w", err)
	}

	if _, err := r.db.ExecContext(ctx, query, args...); err != nil {
		return stack.Errorf("exec context: %w", err)
	}

	return nil
}

// Users returns all users.
func (r *User) Users(
	ctx context.Context,
) ([]storage.User, error) {
	query, args, err := psql.Select(userColumns...).
		From("users").
		ToSql()

	if err != nil {
		return nil, stack.Errorf("build query: %w", err)
	}

	rows, err := r.db.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, stack.Errorf("query context: %w", err)
	}
	defer rows.Close()

	var users []storage.User
	for rows.Next() {
		var user storage.User
		if err := scanUser(rows.Scan, &user); err != nil {
			return nil, stack.Errorf("scan query: %w", err)
		}

		users = append(users, user)
	}

	return users, nil
}

func (r *User) notFound(id string) error {
	return storage.NewNotFoundError("user", id)
}

func scanUser(
	scanFunc func(dest ...interface{}) error,
	u *storage.User,
) error {
	return scanFunc(
		&u.ID,
		&u.RoleID,
		&u.Email,
		&u.PasswordHash,
		&u.FirstName,
		&u.LastName,
		&u.Timestamps.Created,
		&u.Timestamps.Updated,
	)
}
