package postgres

import (
	"context"
	"database/sql"
	"errors"

	sq "github.com/Masterminds/squirrel"
	"github.com/romanyx/guard"
	"github.com/romanyx/stack"
	"gitlab.com/dipress/crmifc/internal/storage"
)

var (
	permissionColumns = []string{"id", "service", "method", "description",
		"created_at", "updated_at"}
	permissionInsertColumns = permissionColumns[:4]
)

// Permission data access object.
type Permission struct {
	db *sql.DB
}

// NewPermission returns ready to use permission's repository.
func NewPermission(db *sql.DB) *Permission {
	guardNewPermission(db)
	p := Permission{
		db: db,
	}

	return &p
}

// guardMewPermission allows to guard NewPermission constructor.
func guardNewPermission(db *sql.DB) {
	guard.MustNotNil(1, "db", db)
}

// Store stores a new permission into database.
func (r *Permission) Store(
	ctx context.Context,
	model *storage.Permission,
) error {
	sql, args, err := psql.Insert("permissions").
		Columns(permissionInsertColumns...).
		Values(
			model.ID,
			model.Service,
			model.Method,
			model.Description,
		).
		ToSql()

	if err != nil {
		return stack.Errorf("build query: %w", err)
	}

	if _, err := r.db.ExecContext(ctx, sql, args...); err != nil {
		return stack.Errorf("exec context: %w", err)
	}

	return nil
}

// ByID return permission by id.
func (r *Permission) ByID(
	ctx context.Context,
	id string,
) (*storage.Permission, error) {
	query, args, err := psql.Select(permissionColumns...).
		From("permissions").
		Where(sq.Eq{"id": id}).
		ToSql()
	if err != nil {
		return nil, stack.Errorf("build query: %w", err)
	}

	var p storage.Permission
	row := r.db.QueryRowContext(ctx, query, args...)
	if err := scanPermission(row.Scan, &p); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, r.notFound(id)
		}

		return nil, stack.Errorf("scan query: %w", err)
	}

	return &p, nil
}

// Update updates a permission by id.
func (r *Permission) Update(
	ctx context.Context,
	model *storage.Permission,
) error {
	query, args, err := psql.Update("permissions").
		SetMap(map[string]interface{}{
			"service":     model.Service,
			"method":      model.Method,
			"description": model.Description,
		}).
		Where(sq.Eq{"id": model.ID}).
		ToSql()

	if err != nil {
		return stack.Errorf("build query: %w", err)
	}

	if _, err := r.db.ExecContext(ctx, query, args...); err != nil {
		return stack.Errorf("exec context: %w", err)
	}

	return nil
}

// Delete deletes a permission by id.
func (r *Permission) Delete(
	ctx context.Context,
	id string,
) error {
	query, args, err := psql.Delete("permissions").
		Where(sq.Eq{"id": id}).
		ToSql()

	if err != nil {
		return stack.Errorf("build query: %w", err)
	}

	if _, err := r.db.ExecContext(ctx, query, args...); err != nil {
		return stack.Errorf("exec context: %w", err)
	}

	return nil
}

// Permissions returns all permissions.
func (r *Permission) Permissions(
	ctx context.Context,
) ([]storage.Permission, error) {
	query, args, err := psql.Select(permissionColumns...).
		From("permissions").
		ToSql()

	if err != nil {
		return nil, stack.Errorf("build query: %w", err)
	}

	rows, err := r.db.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, stack.Errorf("query context: %w", err)
	}
	defer rows.Close()

	var permissions []storage.Permission
	for rows.Next() {
		var permission storage.Permission
		if err := scanPermission(rows.Scan, &permission); err != nil {
			return nil, stack.Errorf("scan query: %w", err)
		}

		permissions = append(permissions, permission)
	}

	return permissions, nil
}

func (r *Permission) notFound(id string) error {
	return storage.NewNotFoundError("permission", id)
}

func scanPermission(
	scanFunc func(dest ...interface{}) error,
	p *storage.Permission,
) error {
	return scanFunc(
		&p.ID,
		&p.Service,
		&p.Method,
		&p.Description,
		&p.Timestamps.Created,
		&p.Timestamps.Updated,
	)
}
