package postgres

import (
	"context"
	"database/sql"
	"errors"

	sq "github.com/Masterminds/squirrel"
	"github.com/romanyx/guard"
	"github.com/romanyx/stack"
	"gitlab.com/dipress/crmifc/internal/storage"
)

var (
	roleColumns       = []string{"id", "name", "description", "created_at", "updated_at"}
	roleInsertColumns = roleColumns[:3]
)

// Role data access object.
type Role struct {
	db *sql.DB
}

// NewRole returns ready to use role's repository.
func NewRole(db *sql.DB) *Role {
	guardNewRole(db)
	r := Role{
		db: db,
	}

	return &r
}

// guardMewRole allows to guard NewRole constructor.
func guardNewRole(db *sql.DB) {
	guard.MustNotNil(1, "db", db)
}

// Store stores a new role into database.
func (r *Role) Store(
	ctx context.Context,
	model *storage.Role,
) error {
	sql, args, err := psql.Insert("roles").
		Columns(roleInsertColumns...).
		Values(
			model.ID,
			model.Name,
			model.Description,
		).
		ToSql()

	if err != nil {
		return stack.Errorf("build query: %w", err)
	}

	if _, err := r.db.ExecContext(ctx, sql, args...); err != nil {
		return stack.Errorf("exec context: %w", err)
	}

	return nil
}

func (r *Role) Unique(
	ctx context.Context,
	name string,
) error {
	query, args, err := psql.Select("count(*)").
		From("roles").
		Where(sq.Eq{"name": name}).
		ToSql()
	if err != nil {
		return stack.Errorf("build query: %w", err)
	}

	var c int
	if err := r.db.QueryRowContext(ctx, query, args...).Scan(&c); err != nil {
		return stack.Errorf("scan: %w", err)
	}

	if c > 0 {
		return storage.ErrNameAlreadyExists
	}

	return nil
}

// ByID return role by id.
func (r *Role) ByID(
	ctx context.Context,
	id string,
) (*storage.Role, error) {
	query, args, err := psql.Select(roleColumns...).
		From("roles").
		Where(sq.Eq{"id": id}).
		ToSql()
	if err != nil {
		return nil, stack.Errorf("build query: %w", err)
	}

	var rl storage.Role
	row := r.db.QueryRowContext(ctx, query, args...)
	if err := scanRole(row.Scan, &rl); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, r.notFound(id)
		}

		return nil, stack.Errorf("scan query: %w", err)
	}

	return &rl, nil
}

// ByName return role by name.
func (r *Role) ByName(
	ctx context.Context,
	name string,
) (*storage.Role, error) {
	query, args, err := psql.Select(roleColumns...).
		From("roles").
		Where(sq.Eq{"name": name}).
		ToSql()
	if err != nil {
		return nil, stack.Errorf("build query: %w", err)
	}

	var rl storage.Role
	row := r.db.QueryRowContext(ctx, query, args...)
	if err := scanRole(row.Scan, &rl); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, r.notFound(name)
		}

		return nil, stack.Errorf("scan query: %w", err)
	}

	return &rl, nil
}

// Update updates a role by id.
func (r *Role) Update(
	ctx context.Context,
	model *storage.Role,
) error {
	query, args, err := psql.Update("roles").
		SetMap(map[string]interface{}{
			"name":        model.Name,
			"description": model.Description,
		}).
		Where(sq.Eq{"id": model.ID}).
		ToSql()

	if err != nil {
		return stack.Errorf("build query: %w", err)
	}

	if _, err := r.db.ExecContext(ctx, query, args...); err != nil {
		return stack.Errorf("exec context: %w", err)
	}

	return nil
}

// Delete deletes a role by id.
func (r *Role) Delete(
	ctx context.Context,
	id string,
) error {
	query, args, err := psql.Delete("roles").
		Where(sq.Eq{"id": id}).
		ToSql()

	if err != nil {
		return stack.Errorf("build query: %w", err)
	}

	if _, err := r.db.ExecContext(ctx, query, args...); err != nil {
		return stack.Errorf("exec context: %w", err)
	}

	return nil
}

// Roles returns all roles.
func (r *Role) Roles(
	ctx context.Context,
) ([]storage.Role, error) {
	query, args, err := psql.Select(roleColumns...).
		From("roles").
		ToSql()

	if err != nil {
		return nil, stack.Errorf("build query: %w", err)
	}

	rows, err := r.db.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, stack.Errorf("query context: %w", err)
	}
	defer rows.Close()

	var roles []storage.Role
	for rows.Next() {
		var role storage.Role
		if err := scanRole(rows.Scan, &role); err != nil {
			return nil, stack.Errorf("scan query: %w", err)
		}

		roles = append(roles, role)
	}

	return roles, nil
}

func (r *Role) notFound(id string) error {
	return storage.NewNotFoundError("role", id)
}

func scanRole(
	scanFunc func(dest ...interface{}) error,
	r *storage.Role,
) error {
	return scanFunc(
		&r.ID,
		&r.Name,
		&r.Description,
		&r.Timestamps.Created,
		&r.Timestamps.Updated,
	)
}
