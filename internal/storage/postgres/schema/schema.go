package schema

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/mattes/migrate"
	"github.com/mattes/migrate/database/postgres"
	"github.com/mattes/migrate/source/go-bindata"
	"github.com/romanyx/stack"
)

// go:generate go-bindata -prefix migrations/ -pkg schema -o migrations.bindata.go migrations/

const (
	source          = "go-bindata"
	driver          = "postgres"
	migrationsTable = "versions"
)

// Checker migration version checker
type Checker struct {
	conn *migrate.Migrate
	last string
}

// NewChecker prepares migration checker.
func NewChecker(conn *sql.DB) (*Checker, error) {
	m, err := newMigration(conn)

	if err != nil {
		return nil, stack.Errorf("new migration: %w", err)
	}

	all := AssetNames()

	var max time.Time
	for _, string := range all {
		t, err := time.Parse("20060102150405", strings.Split(string, "_")[0])
		if err != nil {
			return nil, err
		}

		if t.After(max) {
			max = t
		}
	}

	db := Checker{
		conn: m,
		last: max.Format("20060102150405"),
	}

	return &db, nil
}

// Ping checks migration version.
func (db *Checker) Ping(ctx context.Context) error {
	ver, dirty, err := db.conn.Version()

	if err != nil {
		return stack.Errorf("migration version error: %w", err)
	}

	if dirty {
		return stack.Errorf("migration is dirty, need manual action")
	}

	if !strings.HasPrefix(db.last, fmt.Sprintf("%d", ver)) {
		return stack.Errorf("migration version diff: exist %d last %s", ver, db.last)
	}

	return nil
}

// Migrate migrates schema to given database
// connection.
func Migrate(db *sql.DB) error {
	m, err := newMigration(db)
	if err != nil {
		return stack.Errorf("new migration: %w", err)
	}

	if err := m.Up(); err != nil {
		return stack.Errorf("migrate schema: %w", err)
	}

	return nil
}

func newMigration(db *sql.DB) (*migrate.Migrate, error) {
	r := bindata.Resource(AssetNames(), Asset)

	s, err := bindata.WithInstance(r)
	if err != nil {
		return nil, stack.Errorf("prepare source instance: %w", err)
	}

	cfg := postgres.Config{
		MigrationsTable: migrationsTable,
	}
	d, err := postgres.WithInstance(db, &cfg)
	if err != nil {
		return nil, stack.Errorf("prepare database instance: %w", err)
	}

	m, err := migrate.NewWithInstance(source, s, driver, d)
	if err != nil {
		return nil, stack.Errorf("prepare migrate instance: %w", err)
	}

	return m, nil
}
