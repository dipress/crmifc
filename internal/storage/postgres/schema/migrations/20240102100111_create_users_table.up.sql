CREATE EXTENSION IF NOT EXISTS "citext";

CREATE TABLE IF NOT EXISTS users (
  id            UUID PRIMARY KEY,
  role_id       UUID NOT NULL REFERENCES roles (id),

  email         CITEXT        NOT NULL UNIQUE,
  password_hash	VARCHAR(72)   NOT NULL,
  first_name    VARCHAR(256),
  last_name     VARCHAR(256),
  
  /* timestamps */
  created_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE INDEX IF NOT EXISTS users_email_idx ON users (email);
