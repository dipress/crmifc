CREATE TABLE IF NOT EXISTS admissions (
    role_id         UUID    NOT NULL,
    permission_id   UUID    NOT NULL,
    PRIMARY KEY (role_id, permission_id),
    FOREIGN KEY (role_id) REFERENCES roles(id) ON DELETE CASCADE,
    FOREIGN KEY (permission_id) REFERENCES permissions(id) ON DELETE CASCADE
);