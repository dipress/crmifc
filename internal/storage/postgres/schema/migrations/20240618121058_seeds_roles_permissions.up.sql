/* Inserting necessary permissions */
INSERT INTO permissions(id, service, method, description) 
VALUES (gen_random_uuid(), 'proto.Permission', 'Create', 'Create a permission.');
INSERT INTO permissions(id, service, method, description) 
VALUES (gen_random_uuid(), 'proto.Permission', 'Find', 'Find a permission.');
INSERT INTO permissions(id, service, method, description) 
VALUES (gen_random_uuid(), 'proto.Permission', 'Update', 'Update a permission.');
INSERT INTO permissions(id, service, method, description) 
VALUES (gen_random_uuid(), 'proto.Permission', 'Delete', 'Delete a permission.');
INSERT INTO permissions(id, service, method, description) 
VALUES (gen_random_uuid(), 'proto.Permission', 'Permissions', 'Show all permissions');
INSERT INTO permissions(id, service, method, description) 
VALUES (gen_random_uuid(), 'proto.Role', 'Create', 'Create a role');
INSERT INTO permissions(id, service, method, description) 
VALUES (gen_random_uuid(), 'proto.Role', 'Find', 'Find a role');
INSERT INTO permissions(id, service, method, description) 
VALUES (gen_random_uuid(), 'proto.Role', 'Update', 'Update a role');
INSERT INTO permissions(id, service, method, description) 
VALUES (gen_random_uuid(), 'proto.Role', 'Delete', 'Delete a role');
INSERT INTO permissions(id, service, method, description) 
VALUES (gen_random_uuid(), 'proto.Role', 'Roles', 'Show all roles');
INSERT INTO permissions(id, service, method, description) 
VALUES (gen_random_uuid(), 'proto.User', 'Create', 'Create a user');
INSERT INTO permissions(id, service, method, description) 
VALUES (gen_random_uuid(), 'proto.User', 'Find', 'Find a user');
INSERT INTO permissions(id, service, method, description) 
VALUES (gen_random_uuid(), 'proto.User', 'Update', 'Update a user');
INSERT INTO permissions(id, service, method, description) 
VALUES (gen_random_uuid(), 'proto.User', 'Delete', 'Delete a user');
INSERT INTO permissions(id, service, method, description) 
VALUES (gen_random_uuid(), 'proto.User', 'Users', 'Show all users');
INSERT INTO permissions(id, service, method, description) 
VALUES (gen_random_uuid(), 'proto.Admission', 'Add', 'add new permission to role');
INSERT INTO permissions(id, service, method, description) 
VALUES (gen_random_uuid(), 'proto.Admission', 'Remove', 'Removes the permission by role');


/* Creating a role and adding to it all permissions */
DO $$
	DECLARE rid uuid;
	DECLARE row record;
BEGIN
	INSERT INTO roles(id, name, description) VALUES(gen_random_uuid(), 'Admin', 'Administrators')
	RETURNING id INTO rid;

	FOR row IN SELECT * FROM permissions LOOP
		INSERT INTO admissions(role_id, permission_id) VALUES (rid, row.id);
	END LOOP;
END $$