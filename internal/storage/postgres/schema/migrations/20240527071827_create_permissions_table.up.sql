CREATE TABLE IF NOT EXISTS permissions (
    id              UUID PRIMARY KEY,

    service         VARCHAR(256) NOT NULL,
    method          VARCHAR(256) NOT NULL,
    description     VARCHAR(256) NOT NULL,
    
    /* timestamps */
    created_at        TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at        TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE INDEX IF NOT EXISTS permission_action_idx ON permissions (service);
CREATE INDEX IF NOT EXISTS permission_method_idx ON permissions (method);