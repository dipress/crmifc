package schema

import (
	"context"
	"database/sql"
	"log"
	"os"
	"testing"
	"time"

	"github.com/ory/dockertest"
	"gitlab.com/dipress/crmifc/internal/docker"
)

var (
	db *sql.DB
)

func TestMain(m *testing.M) {
	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("create new pool: %v", err)
	}

	pgDocker, err := docker.NewPostgres(pool, docker.SetConnTimeout(3*time.Minute))
	if err != nil {
		log.Fatalf("start postgres image: %v", err)
	}
	db = pgDocker.DB

	code := m.Run()

	db.Close()
	if err := pool.Purge(pgDocker.Resource); err != nil {
		log.Fatalf("purge postgres image: %v", err)
	}

	os.Exit(code)
}

func TestMigrate(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode")
	}

	t.Log("Given the need to migrate database schema")
	{
		t.Log("\tWhen starting new postgres docker instance")
		{
			err := Migrate(db)

			expected(t, nilError(t, err), "Should migrate schema")

			ch, _ := NewChecker(db)
			err = ch.Ping(context.Background())

			expected(t, nilError(t, err), "Checker should be clear")
		}
	}
}

func expected(t *testing.T, expected bool, text string) {
	t.Helper()
	if expected {
		t.Logf("+\t\t%s", text)
		return
	}
	t.Errorf("-\t\t%s", text)
}

func nilError(t *testing.T, err error) bool {
	t.Helper()

	if err != nil {
		t.Errorf("\tunexpected error: %v", err)
		return false
	}

	return true
}
