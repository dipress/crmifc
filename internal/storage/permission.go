package storage

type Permission struct {
	ID          string
	Service     string
	Method      string
	Description string

	Timestamps Timestamps
}
