package storage

import (
	"errors"
	"fmt"
)

// ErrNotFound returns when resource not found
// in storage.
var ErrNotFound = errors.New("not found")

// NotFoundError allows to specify not founded
// resource for storage.
type NotFoundError struct {
	Kind, Key string

	origin error
}

// NewNotFoundError prepares not found error.
func NewNotFoundError(kind, key string) error {
	e := NotFoundError{
		Kind:   kind,
		Key:    key,
		origin: ErrNotFound,
	}

	return e
}

// Error implements error interface.
func (e NotFoundError) Error() string {
	return fmt.Sprintf("%s with key %s not found", e.Kind, e.Key)
}

// Unwrap unwraps original error.
func (e NotFoundError) Unwrap() error {
	return e.origin
}
