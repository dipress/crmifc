package storage

import "errors"

var (
	// ErrNameAlreadyExists returns when trying to
	// create role with the same name.
	ErrNameAlreadyExists = errors.New("name already exists")
)

type Role struct {
	ID          string
	Name        string
	Description string

	Timestamps Timestamps
}
