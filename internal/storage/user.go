package storage

import (
	"errors"
	"time"
)

var (
	// ErrEmailAlreadyExists returns when trying to
	// create user with the same email.
	ErrEmailAlreadyExists = errors.New("email already exists")
)

type User struct {
	ID           string
	RoleID       string
	Email        string
	PasswordHash string
	FirstName    string
	LastName     string

	Timestamps Timestamps
}

type Timestamps struct {
	Created, Updated time.Time
}
