package auth

import (
	"context"
	"crypto/rsa"
	"encoding/base64"
	"errors"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/romanyx/guard"
	"github.com/romanyx/stack"
	"gitlab.com/dipress/crmifc/internal/storage"
	"golang.org/x/crypto/scrypt"
)

var (
	// ErrUserMissing returns when user missing from context.
	ErrUserMissing = errors.New("user missing from context")
	// ErrTokenInvalid returns when jwt token is invalid.
	ErrTokenInvalid = errors.New("token is invalid")
)

const (
	accessTokenExpireAfter  = 24 * time.Hour
	refreshTokenExpireAfter = 24 * 3 * time.Hour
)

const algorithm = "RS256"

type ctxKey int

// Tokens contains access and refresh JWT tokens.
type Tokens struct {
	Access  string
	Refresh string
}

const userKey ctxKey = 1

// Claims represents the authorization claims transmitted via a JWT.
type Claims struct {
	jwt.StandardClaims
}

// newClaims constructs a Claims value for the identified user. The Claims
// expire within a specified duration of the provided time. Additional fields
// of the Claims can be set after calling NewClaims is desired.
func newClaims(
	subject string,
	now time.Time,
	expires time.Duration,
) Claims {
	c := Claims{
		StandardClaims: jwt.StandardClaims{
			Subject:   subject,
			IssuedAt:  now.Unix(),
			ExpiresAt: now.Add(expires).Unix(),
		},
	}

	return c
}

// Valid is called during the parsing of a token.
func (c Claims) Valid() error {
	if err := c.StandardClaims.Valid(); err != nil {
		return stack.Errorf("validating standard claims: %w", err)
	}
	return nil
}

// Authenticator is used to authenticate clients. It can generate a token for a
// set of user claims and recreate the claims by parsing the token.
type Authenticator struct {
	key    *rsa.PrivateKey
	public *rsa.PublicKey
	parser *jwt.Parser
	salt   []byte
}

// NewAuthenticator creates an *Authenticator for use.
func NewAuthenticator(
	key *rsa.PrivateKey,
	salt []byte,
) (*Authenticator, error) {
	guardNewAuthenticator(key, salt)

	parser := jwt.Parser{
		ValidMethods: []string{algorithm},
	}

	public, ok := key.Public().(*rsa.PublicKey)
	if !ok {
		return nil, errors.New("assert public key")
	}

	a := Authenticator{
		key:    key,
		public: public,
		parser: &parser,
		salt:   salt,
	}

	return &a, nil
}

// guardNewMAuthenticator allows to guard NewAuthenticator constructor.
func guardNewAuthenticator(
	key *rsa.PrivateKey,
	salt []byte,
) {
	guard.MustNotNil(1, "key", key)
	guard.MustNotNil(1, "salt", salt)
}

// PasswordHash generates password hash.
func (a *Authenticator) PasswordHash(password string) (string, error) {
	dk, err := scrypt.Key([]byte(password), a.salt, 32768, 8, 1, 32)
	if err != nil {
		return "", stack.Errorf("key: %w", err)
	}

	return base64.StdEncoding.EncodeToString(dk), nil
}

// Tokens generates a signed JWT token string representing the user Claims.
func (a *Authenticator) Tokens(
	ctx context.Context,
	id string,
	creates time.Time,
) (*Tokens, error) {
	accessClaims := newClaims(id, creates, accessTokenExpireAfter)
	method := jwt.GetSigningMethod(algorithm)
	accessTkn := jwt.NewWithClaims(method, accessClaims)

	access, err := accessTkn.SignedString(a.key)
	if err != nil {
		return nil, stack.Errorf("signing access token: %w", err)
	}

	refreshClaims := newClaims(id, creates, refreshTokenExpireAfter)
	refreshTkn := jwt.NewWithClaims(method, refreshClaims)

	refresh, err := refreshTkn.SignedString(a.key)
	if err != nil {
		return nil, stack.Errorf("signing refresh token: %w", err)
	}

	var tokens Tokens
	tokens.Access = access
	tokens.Refresh = refresh

	return &tokens, nil
}

// ParseClaims recreates the Claims that were used to generate a token. It
// verifies that the token was signed using our key.
func (a *Authenticator) ParseClaims(
	ctx context.Context,
	tknStr string,
) (Claims, error) {
	f := func(t *jwt.Token) (interface{}, error) {
		return a.public, nil
	}

	var claims Claims
	tkn, err := a.parser.ParseWithClaims(tknStr, &claims, f)
	if err != nil {
		return Claims{}, ErrTokenInvalid
	}

	if !tkn.Valid {
		return Claims{}, ErrTokenInvalid
	}

	return claims, nil
}

// UserFromContext return user from context.
func UserFromContext(
	ctx context.Context,
) (*storage.User, error) {
	user, ok := ctx.Value(userKey).(*storage.User)
	if !ok {
		return nil, ErrUserMissing
	}

	return user, nil
}

// ContextWithUser puts user into context.
func ContextWithUser(
	ctx context.Context,
	user *storage.User,
) context.Context {
	c := context.WithValue(ctx, userKey, user)
	return c
}
