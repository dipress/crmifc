package init

import (
	"database/sql"
	"errors"

	"gitlab.com/dipress/crmifc/internal/auth"
	"gitlab.com/dipress/crmifc/internal/logger"
)

// Components contains required for commands components,
// like logger, database, etc.
type Components struct {
	db     *sql.DB
	auth   *auth.Authenticator
	logger *logger.Logger

	teardowns []func() error
}

// DB returns db connection.
func (c *Components) DB() *sql.DB {
	if c.db == nil {
		c.logger.Fatal(errors.New("db is nil"), nil)

	}
	return c.db
}

// Auth returns auth.
func (c *Components) Auth() *auth.Authenticator {
	if c.auth == nil {
		c.logger.Fatal(errors.New("auth is nil"), nil)
	}
	return c.auth
}

// Logger returns logger.
func (c *Components) Logger() *logger.Logger {
	if c.logger == nil {
		c.logger.Fatal(errors.New("logger is nil"), nil)
	}
	return c.logger
}

// Teardown allows to teardown infrastructure.
func (c *Components) Teardown() {
	for _, teardown := range c.teardowns {
		if err := teardown(); err != nil {
			c.logger.Fatal(err, nil)
		}
	}
}

func (c *Components) addTeardown(teardown func() error) {
	c.teardowns = append(c.teardowns, teardown)
}

// Option allows to change Infrastructure initializer
// default behavior.
type Option func(*Components) error

// Infrastructure initialize componets can call panic
// or logger fatals when failing to initialize on of
// required components.
func Infrastructure(options ...Option) *Components {
	c := Components{}

	for _, option := range options {
		if err := option(&c); err != nil {
			c.logger.Fatal(err, nil)
		}
	}

	return &c
}
