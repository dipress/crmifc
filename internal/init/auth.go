package init

import (
	"github.com/golang-jwt/jwt"
	"github.com/kelseyhightower/envconfig"
	"github.com/romanyx/stack"
	"gitlab.com/dipress/crmifc/internal/auth"
)

type authSpec struct {
	Key  string `required:"true"`
	Salt string `required:"true"`
}

// WithAut enables authenticator in infrastructure.
func WithAuth(c *Components) error {
	var s authSpec
	if err := envconfig.Process("auth", &s); err != nil {
		return stack.Errorf("parse env: %w", err)
	}

	key, err := jwt.ParseRSAPrivateKeyFromPEM([]byte(s.Key))
	if err != nil {
		return stack.Errorf("parsing auth private key: %w", err)
	}

	ac, err := auth.NewAuthenticator(key, []byte(s.Salt))
	if err != nil {
		return stack.Errorf("authenticator: %w", err)
	}

	c.auth = ac

	return nil
}
