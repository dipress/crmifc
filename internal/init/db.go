package init

import (
	"database/sql"
	"fmt"

	"github.com/kelseyhightower/envconfig"
	"github.com/romanyx/stack"
)

type databaseSpec struct {
	Host     string `default:"pg"`
	Port     string `default:"5432"`
	User     string `default:"user"`
	Password string `default:"pass"`
	Database string `default:"db"`
	SSLMode  string `default:"disable" envconfig:"ssl_mode"`
}

func (s *databaseSpec) URI() string {
	return fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		s.Host,
		s.Port,
		s.User,
		s.Password,
		s.Database,
		s.SSLMode,
	)
}

// WithDB enables database initialization.
func WithDB(c *Components) error {
	var s databaseSpec
	if err := envconfig.Process("postgres", &s); err != nil {
		return stack.Errorf("parse env: %w", err)
	}

	db, err := prepareDB(s.URI())
	if err != nil {
		return stack.Errorf("prepare db: %w", err)
	}

	if err := db.Ping(); err != nil {
		return stack.Errorf("ping db: %w", err)
	}

	c.db = db
	c.addTeardown(db.Close)

	return nil
}

func prepareDB(dsn string) (*sql.DB, error) {
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		return nil, stack.Errorf("open: %w", err)
	}
	return db, nil
}
