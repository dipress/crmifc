package init

import (
	"os"

	"github.com/kelseyhightower/envconfig"
	"github.com/romanyx/stack"
	"gitlab.com/dipress/crmifc/internal/logger"
)

type loggerSpec struct {
	SensitiveFields []string `default:"password, token"`
}

// WithLogger allows to initialize logger component
// for infrastructure.
func WithLogger(c *Components) error {
	var s loggerSpec
	if err := envconfig.Process("logger", &s); err != nil {
		return stack.Errorf("parse environment variables: %w", err)
	}

	logger, err := logger.New(
		logger.WithSensitiveFields(s.SensitiveFields),
		logger.SetOutputter(os.Stdout),
	)

	logger.SetLevel("info")

	if err != nil {
		return stack.Errorf("logger: %w", err)
	}

	c.logger = logger

	return nil
}
