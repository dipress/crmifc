package grpc

import (
	"context"
	"errors"
	"unicode"

	ozzo "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/romanyx/stack"
	"gitlab.com/dipress/crmifc/internal/adapter/grpc/validation"
	"gitlab.com/dipress/crmifc/internal/auth"
	"gitlab.com/dipress/crmifc/internal/domain"
	"gitlab.com/dipress/crmifc/internal/proto"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// userDL user domain logic.
type userDL interface {
	Create(context.Context, *domain.NewUserDTO) (*domain.UserDTO, error)
	Find(context.Context, string) (*domain.UserDTO, error)
	Update(context.Context, *domain.UpdateUserDTO) (*domain.UserDTO, error)
	Delete(context.Context, string) error
	Users(context.Context) ([]domain.UserDTO, error)
}

// user server.
type user struct {
	dl userDL

	proto.UnimplementedUserServer
}

// Create creates a user.
func (s *user) Create(
	ctx context.Context,
	req *proto.UserCreateRequest,
) (*proto.UserResponse, error) {
	_, err := auth.UserFromContext(ctx)
	if err != nil {
		return nil, stack.Errorf("user from context: %w", err)
	}

	if err := validateCreateUser(req); err != nil {
		return nil, stack.Errorf("validate: %w", err)
	}

	var dto domain.NewUserDTO
	dto.Role_ID = req.RoleId
	dto.Email = req.Email
	dto.Password = req.Password
	dto.FistName = req.FirstName
	dto.LastName = req.LastName

	user, err := s.dl.Create(ctx, &dto)
	if err != nil {
		switch {
		case errors.Is(err, domain.ErrEmailAlreadyExists):
			return nil, status.Error(
				codes.AlreadyExists,
				err.Error(),
			)
		default:
			return nil, stack.Errorf("create: %w", err)
		}
	}

	return userResponse(user), nil
}

func validateCreateUser(req *proto.UserCreateRequest) error {
	ves := validation.NewErrors()
	uuid := uuidVerifier{}
	password := passwordVerifier{}

	if err := ozzo.Validate(
		req.RoleId,
		ozzo.Required,
		ozzo.By(uuid.verify),
	); err != nil {
		ves.Details["role_id"] = err.Error()
	}

	if err := ozzo.Validate(
		req.Email,
		ozzo.Required,
		is.Email,
	); err != nil {
		ves.Details["email"] = err.Error()
	}

	if err := ozzo.Validate(
		req.Password,
		ozzo.Required,
		ozzo.By(password.verify),
	); err != nil {
		ves.Details["password"] = err.Error()
	}

	if len(ves.Details) > 0 {
		return ves
	}

	return nil
}

func (s *user) Find(
	ctx context.Context,
	req *proto.UserFindRequest,
) (*proto.UserResponse, error) {
	_, err := auth.UserFromContext(ctx)
	if err != nil {
		return nil, stack.Errorf("user from context: %w", err)
	}

	if err := validateFindUser(req); err != nil {
		return nil, stack.Errorf("validate: %w", err)
	}

	user, err := s.dl.Find(ctx, req.Id)
	if err != nil {
		return nil, stack.Errorf("find: %w", err)
	}

	return userResponse(user), nil
}

func validateFindUser(req *proto.UserFindRequest) error {
	ves := validation.NewErrors()
	uuid := uuidVerifier{}

	if err := ozzo.Validate(
		req.Id,
		ozzo.Required,
		ozzo.By(uuid.verify),
	); err != nil {
		ves.Details["id"] = err.Error()
	}

	if len(ves.Details) > 0 {
		return ves
	}

	return nil
}

func (s *user) Update(
	ctx context.Context,
	req *proto.UserUpdateRequest,
) (*proto.UserResponse, error) {
	_, err := auth.UserFromContext(ctx)
	if err != nil {
		return nil, stack.Errorf("user from context: %w", err)
	}

	if err := validateUpdateUser(req); err != nil {
		return nil, stack.Errorf("validate: %w", err)
	}

	var dto domain.UpdateUserDTO
	dto.ID = req.Id
	dto.Role_ID = req.RoleId
	dto.Email = req.Email
	dto.FirstName = req.FirstName
	dto.LastName = req.LastName

	user, err := s.dl.Update(ctx, &dto)
	if err != nil {
		switch {
		case errors.Is(err, domain.ErrUserNotFound):
			return nil, status.Error(
				codes.NotFound,
				err.Error(),
			)

		case errors.Is(err, domain.ErrRoleNotFound):
			return nil, status.Error(
				codes.NotFound,
				err.Error(),
			)
		default:
			return nil, stack.Errorf("update: %w", err)
		}
	}

	return userResponse(user), nil
}

func validateUpdateUser(req *proto.UserUpdateRequest) error {
	ves := validation.NewErrors()
	uuid := uuidVerifier{}

	if err := ozzo.Validate(
		req.Id,
		ozzo.Required,
		ozzo.By(uuid.verify),
	); err != nil {
		ves.Details["id"] = err.Error()
	}

	if err := ozzo.Validate(
		req.RoleId,
		ozzo.Required,
		ozzo.By(uuid.verify),
	); err != nil {
		ves.Details["role_id"] = err.Error()
	}

	if err := ozzo.Validate(
		req.Email,
		ozzo.Required,
		is.Email,
	); err != nil {
		ves.Details["email"] = err.Error()
	}

	if len(ves.Details) > 0 {
		return ves
	}

	return nil
}

// Delete deletes user by id.
func (s *user) Delete(
	ctx context.Context,
	req *proto.UserDeleteRequest,
) (*emptypb.Empty, error) {
	_, err := auth.UserFromContext(ctx)
	if err != nil {
		return nil, stack.Errorf("user from context: %w", err)
	}

	if err := validateDeleteUser(req); err != nil {
		return nil, stack.Errorf("validate: %w", err)
	}

	if err := s.dl.Delete(ctx, req.Id); err != nil {
		return nil, stack.Errorf("delete: %w", err)
	}

	return new(emptypb.Empty), nil
}

func validateDeleteUser(req *proto.UserDeleteRequest) error {
	ves := validation.NewErrors()
	uuid := uuidVerifier{}

	if err := ozzo.Validate(
		req.Id,
		ozzo.Required,
		ozzo.By(uuid.verify),
	); err != nil {
		ves.Details["id"] = err.Error()
	}

	if len(ves.Details) > 0 {
		return ves
	}

	return nil
}

// Users return list of all users.
func (s *user) Users(
	ctx context.Context,
	in *emptypb.Empty,
) (*proto.UsersResponse, error) {
	_, err := auth.UserFromContext(ctx)
	if err != nil {
		return nil, stack.Errorf("user from context: %w", err)
	}
	users, err := s.dl.Users(ctx)
	if err != nil {
		return nil, stack.Errorf("users: %w", err)
	}

	var usr []*proto.UserResponse
	for _, user := range users {
		usr = append(usr, userResponse(&user))
	}

	return &proto.UsersResponse{
		Users: usr,
	}, nil
}

type passwordVerifier struct {
	Upper   bool
	Number  bool
	Special bool
	Length  int
}

// nolint: gocyclo
func (p *passwordVerifier) verify(value interface{}) error {
	password, ok := value.(string)

	if !ok {
		return errors.New("value must be a string")
	}

	for _, char := range password {
		switch {
		case unicode.IsUpper(char):
			p.Upper = true
			p.Length++
		case unicode.IsNumber(char):
			p.Number = true
			p.Length++
		case unicode.IsPunct(char) || unicode.IsSymbol(char):
			p.Special = true
			p.Length++
		default:
			p.Length++
		}
	}

	if !p.Upper || !p.Number || !p.Special || p.Length < 8 {
		return errors.New("password is invalid")
	}

	return nil
}

func userResponse(u *domain.UserDTO) *proto.UserResponse {
	resp := proto.UserResponse{
		Id:        u.ID,
		RoleId:    u.Role_ID,
		Email:     u.Email,
		FirstName: u.FirstName,
		LastName:  u.LastName,
		Timestamps: &proto.Timestamps{
			Created: timestamppb.New(u.Created),
			Updated: timestamppb.New(u.Updated),
		},
	}

	return &resp
}
