package grpc

import (
	"context"

	ozzo "github.com/go-ozzo/ozzo-validation"
	"github.com/romanyx/stack"
	"gitlab.com/dipress/crmifc/internal/adapter/grpc/validation"
	"gitlab.com/dipress/crmifc/internal/auth"
	"gitlab.com/dipress/crmifc/internal/domain"
	"gitlab.com/dipress/crmifc/internal/proto"
	"google.golang.org/protobuf/types/known/emptypb"
)

// admissionDL permission domain logic.
type admissionDL interface {
	Create(context.Context, *domain.NewAdmissionDTO) (*domain.AdmissionDTO, error)
	Delete(context.Context, *domain.DeleteAdmissionDTO) error
	Admissions(context.Context) ([]domain.AdmissionsDTO, error)
}

type admission struct {
	dl admissionDL

	proto.UnimplementedAdmissionServer
}

// Add added admission to role.
func (s *admission) Add(
	ctx context.Context,
	req *proto.AdmissionAddRequest,
) (*proto.AdmissionResponse, error) {
	_, err := auth.UserFromContext(ctx)
	if err != nil {
		return nil, stack.Errorf("user from context: %w", err)
	}

	if err := validateAddAdmission(req); err != nil {
		return nil, stack.Errorf("validation: %w", err)
	}

	var dto domain.NewAdmissionDTO
	dto.RoleID = req.RoleId
	dto.PermissionID = req.PermissionId

	admission, err := s.dl.Create(ctx, &dto)
	if err != nil {
		return nil, stack.Errorf("create: %w", err)
	}

	return admissionResponse(admission), nil
}

func validateAddAdmission(req *proto.AdmissionAddRequest) error {
	ves := validation.NewErrors()
	uuid := uuidVerifier{}

	if err := ozzo.Validate(
		req.RoleId,
		ozzo.Required,
		ozzo.By(uuid.verify),
	); err != nil {
		ves.Details["role_id"] = err.Error()
	}

	if err := ozzo.Validate(
		req.PermissionId,
		ozzo.Required,
		ozzo.By(uuid.verify),
	); err != nil {
		ves.Details["permission_id"] = err.Error()
	}

	if len(ves.Details) > 0 {
		return ves
	}

	return nil
}

// Remove removes admission from role.
func (s *admission) Remove(
	ctx context.Context,
	req *proto.AdmissionRemoveRequest,
) (*emptypb.Empty, error) {
	_, err := auth.UserFromContext(ctx)
	if err != nil {
		return nil, stack.Errorf("user from context: %w", err)
	}

	if err := validateRemoveAdmission(req); err != nil {
		return nil, stack.Errorf("validate: %w", err)
	}

	var dto domain.DeleteAdmissionDTO
	dto.RoleID = req.RoleId
	dto.PermissionID = req.PermissionId

	if err := s.dl.Delete(ctx, &dto); err != nil {
		return nil, stack.Errorf("delete: %w", err)
	}

	return new(emptypb.Empty), nil
}

func validateRemoveAdmission(req *proto.AdmissionRemoveRequest) error {
	ves := validation.NewErrors()
	uuid := uuidVerifier{}

	if err := ozzo.Validate(
		req.RoleId,
		ozzo.Required,
		ozzo.By(uuid.verify),
	); err != nil {
		ves.Details["role_id"] = err.Error()
	}

	if err := ozzo.Validate(
		req.PermissionId,
		ozzo.Required,
		ozzo.By(uuid.verify),
	); err != nil {
		ves.Details["permission_id"] = err.Error()
	}

	if len(ves.Details) > 0 {
		return ves
	}

	return nil
}

// Admissions return all admissions.
func (s *admission) Admissions(
	ctx context.Context,
	in *emptypb.Empty,
) (*proto.AdmissionsResponse, error) {
	_, err := auth.UserFromContext(ctx)
	if err != nil {
		return nil, stack.Errorf("user from context: %w", err)
	}

	admissions, err := s.dl.Admissions(ctx)
	if err != nil {
		return nil, stack.Errorf("admissions: %w", err)
	}

	var adms []*proto.Admissions
	for _, adm := range admissions {
		adms = append(adms, admissionsResponse(&adm))
	}

	return &proto.AdmissionsResponse{
		Admissions: adms,
	}, nil

}

func admissionResponse(a *domain.AdmissionDTO) *proto.AdmissionResponse {
	resp := proto.AdmissionResponse{
		RoleId:       a.RoleID,
		PermissionId: a.PermissionID,
	}

	return &resp
}

func admissionsResponse(a *domain.AdmissionsDTO) *proto.Admissions {
	resp := proto.Admissions{
		RoleId:       a.RoleID,
		PermissionId: a.PermissionID,
		Name:         a.Name,
		Service:      a.Service,
		Method:       a.Method,
		Description:  a.Description,
	}

	return &resp
}
