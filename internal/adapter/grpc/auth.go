package grpc

import (
	"context"
	"errors"

	ozzo "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/romanyx/stack"
	"gitlab.com/dipress/crmifc/internal/adapter/grpc/validation"
	"gitlab.com/dipress/crmifc/internal/auth"
	"gitlab.com/dipress/crmifc/internal/domain"
	"gitlab.com/dipress/crmifc/internal/proto"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// authDL auth domain logic.
type authDL interface {
	Login(context.Context, string, string) (*domain.CredentialDTO, error)
	Refresh(context.Context, string) (*domain.CredentialDTO, error)
}

// authentication server.
type authentication struct {
	dl authDL

	proto.UnimplementedAuthServer
}

// SignIn sign in request.
func (s *authentication) SignIn(
	ctx context.Context,
	req *proto.SignInRequest,
) (*proto.TokensResponse, error) {
	if err := validateSignIn(req); err != nil {
		return nil, stack.Errorf("validate: %w", err)
	}

	cred, err := s.dl.Login(ctx, req.Email, req.Password)
	if err != nil {
		switch {
		case errors.Is(err, domain.ErrEmailOrPasswordWrong):
			return nil, status.Error(
				codes.InvalidArgument,
				err.Error(),
			)
		default:
			return nil, stack.Errorf("login: %w", err)
		}
	}

	return tokenResponse(cred), nil
}

func validateSignIn(req *proto.SignInRequest) error {
	ves := validation.NewErrors()

	if err := ozzo.Validate(
		req.Email,
		ozzo.Required,
		is.Email,
	); err != nil {
		ves.Details["email"] = err.Error()
	}

	if err := ozzo.Validate(
		req.Password,
		ozzo.Required,
	); err != nil {
		ves.Details["password"] = err.Error()
	}

	if len(ves.Details) > 0 {
		return ves
	}

	return nil
}

// RefreshToken handles refresh token request.
func (s *authentication) RefreshToken(
	ctx context.Context,
	req *proto.RefreshTokenRequest,
) (*proto.TokensResponse, error) {
	if err := validateRefreshToken(req); err != nil {
		return nil, stack.Errorf("validate: %w", err)
	}

	cred, err := s.dl.Refresh(ctx, req.Token)
	if err != nil {
		switch {
		case errors.Is(err, auth.ErrTokenInvalid):
			return nil, status.Error(
				codes.InvalidArgument,
				err.Error(),
			)
		default:
			return nil, stack.Errorf("refresh token: %w", err)
		}
	}

	return tokenResponse(cred), nil
}

func tokenResponse(c *domain.CredentialDTO) *proto.TokensResponse {
	resp := proto.TokensResponse{
		AccessToken:  c.Access,
		RefreshToken: c.Refresh,
	}

	return &resp
}

func validateRefreshToken(req *proto.RefreshTokenRequest) error {
	ves := validation.NewErrors()
	if err := ozzo.Validate(
		req.Token,
		ozzo.Required,
	); err != nil {
		ves.Details["token"] = err.Error()
	}

	if len(ves.Details) > 0 {
		return ves
	}

	return nil
}
