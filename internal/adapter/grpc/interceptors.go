package grpc

import (
	"context"
	"errors"

	"github.com/romanyx/stack"
	"gitlab.com/dipress/crmifc/internal/adapter/grpc/validation"
	"gitlab.com/dipress/crmifc/internal/auth"
	"gitlab.com/dipress/crmifc/internal/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

var (
	errSkipAuth = errors.New("skip auth")
)


type userDAO interface {
	ByID(context.Context, string) (*storage.User, error)
}

type authenticator interface {
	ParseClaims(
		ctx context.Context,
		tknStr string,
	) (auth.Claims, error)
}

func authUnaryInterceptor(
	a authenticator,
	users userDAO,
) grpc.UnaryServerInterceptor {
	f := func(
		ctx context.Context,
		req interface{},
		info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler,
	) (interface{}, error) {
		ctx, err := authenticate(ctx, a, users)
		if err != nil && !errors.Is(err, errSkipAuth) {
			return nil, stack.Errorf("auth: %w", err)
		}

		if errors.Is(err, errSkipAuth) {
			return handler(ctx, req)
		}

		return handler(ctx, req)
	}

	return f
}

func authenticate(
	ctx context.Context,
	a authenticator,
	users userDAO,
) (context.Context, error) {
	token, ok := getToken(ctx)
	if !ok {
		return ctx, errSkipAuth
	}

	claims, err := a.ParseClaims(ctx, token)
	if err != nil {
		return ctx, status.Error(
			codes.Unauthenticated,
			"token is invalid",
		)
	}

	user, err := users.ByID(ctx, claims.StandardClaims.Subject)
	if err != nil {
		return ctx, stack.Errorf("by id: %w", err)
	}

	ctx = auth.ContextWithUser(ctx, user)

	return ctx, err
}

func getToken(ctx context.Context) (string, bool) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return "", false
	}

	token, ok := md["token"]
	if !ok {
		return "", false
	}

	if len(token) > 1 {
		return "", false
	}

	return token[0], true
}


func logUnaryInterceptor(
	logger Logger,
) grpc.UnaryServerInterceptor {
	f := func(
		ctx context.Context,
		req interface{},
		info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler,
	) (interface{}, error) {
		resp, err := handler(ctx, req)
		if _, ok := status.FromError(err); ok {
			return resp, err
		}

		if err != nil {
			return handleError(err, logger)
		}

		return resp, nil
	}

	return f
}

func handleError(
	err error,
	logger Logger,
) (interface{}, error) {
	var nf storage.NotFoundError
	var vr validation.Errors
	switch {
	case errors.As(err, &nf):
		return nil, status.Error(
			codes.NotFound,
			nf.Error(),
		)
	case errors.As(err, &vr):
		return nil, vr.Err()
	case errors.Is(err, auth.ErrTokenInvalid) || errors.Is(err, auth.ErrUserMissing):
		return nil, status.Error(
			codes.Unauthenticated,
			"unauthenticated",
		)
	default:
		logger.Error(err, nil)

		return nil, status.Error(
			codes.Internal,
			"internal error",
		)
	}
}
