package grpc

import (
	"context"

	ozzo "github.com/go-ozzo/ozzo-validation"
	"github.com/romanyx/stack"
	"gitlab.com/dipress/crmifc/internal/adapter/grpc/validation"
	"gitlab.com/dipress/crmifc/internal/auth"
	"gitlab.com/dipress/crmifc/internal/domain"
	"gitlab.com/dipress/crmifc/internal/proto"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// permissionDL permission domain logic.
type permissionDL interface {
	Create(context.Context, *domain.NewPermissionDTO) (*domain.PermissionDTO, error)
	Find(context.Context, string) (*domain.PermissionDTO, error)
	Update(context.Context, *domain.UpdatePermissionDTO) (*domain.PermissionDTO, error)
	Delete(context.Context, string) error
	Permissions(context.Context) ([]domain.PermissionDTO, error)
}

type permission struct {
	dl permissionDL

	proto.UnimplementedPermissionServer
}

// Create creates a permission
func (s *permission) Create(
	ctx context.Context,
	req *proto.PermissionCreateRequest,
) (*proto.PermissionResponse, error) {
	_, err := auth.UserFromContext(ctx)
	if err != nil {
		return nil, stack.Errorf("user from context: %w", err)
	}

	if err := validateCreatePermission(req); err != nil {
		return nil, stack.Errorf("validate: %w", err)
	}

	var dto domain.NewPermissionDTO
	dto.Service = req.Service
	dto.Method = req.Method
	dto.Description = req.Description

	permission, err := s.dl.Create(ctx, &dto)
	if err != nil {
		return nil, stack.Errorf("create: %w", err)
	}

	return permissionResponse(permission), nil
}

func validateCreatePermission(req *proto.PermissionCreateRequest) error {
	ves := validation.NewErrors()

	if err := ozzo.Validate(
		req.Service,
		ozzo.Required,
		ozzo.Length(1, 256),
	); err != nil {
		ves.Details["service"] = err.Error()
	}

	if err := ozzo.Validate(
		req.Method,
		ozzo.Required,
		ozzo.Length(1, 256),
	); err != nil {
		ves.Details["method"] = err.Error()
	}

	if err := ozzo.Validate(
		req.Description,
		ozzo.Required,
		ozzo.Length(1, 256),
	); err != nil {
		ves.Details["description"] = err.Error()
	}

	if len(ves.Details) > 0 {
		return ves
	}

	return nil
}

// Find finds permission by id.
func (s *permission) Find(
	ctx context.Context,
	req *proto.PermissionFindRequest,
) (*proto.PermissionResponse, error) {
	_, err := auth.UserFromContext(ctx)
	if err != nil {
		return nil, stack.Errorf("user from context: %w", err)
	}

	if err := validateFindPermission(req); err != nil {
		return nil, stack.Errorf("validate: %w", err)
	}

	permission, err := s.dl.Find(ctx, req.Id)
	if err != nil {
		return nil, stack.Errorf("find: %w", err)
	}

	return permissionResponse(permission), nil
}

func validateFindPermission(req *proto.PermissionFindRequest) error {
	ves := validation.NewErrors()
	uuid := uuidVerifier{}

	if err := ozzo.Validate(
		req.Id,
		ozzo.Required,
		ozzo.By(uuid.verify),
	); err != nil {
		ves.Details["id"] = err.Error()
	}

	if len(ves.Details) > 0 {
		return ves
	}

	return nil
}

// Update updates permission by id.
func (s *permission) Update(
	ctx context.Context,
	req *proto.PermissionUpdateRequest,
) (*proto.PermissionResponse, error) {
	_, err := auth.UserFromContext(ctx)
	if err != nil {
		return nil, stack.Errorf("user from context: %w", err)
	}

	if err := validatePermissionUpdate(req); err != nil {
		return nil, stack.Errorf("validate: %w", err)
	}

	var dto domain.UpdatePermissionDTO
	dto.ID = req.Id
	dto.Service = req.Service
	dto.Method = req.Method
	dto.Description = req.Description

	permission, err := s.dl.Update(ctx, &dto)
	if err != nil {
		return nil, stack.Errorf("update: %w", err)
	}

	return permissionResponse(permission), nil
}

func validatePermissionUpdate(req *proto.PermissionUpdateRequest) error {
	ves := validation.NewErrors()
	uuid := uuidVerifier{}

	if err := ozzo.Validate(
		req.Id,
		ozzo.Required,
		ozzo.By(uuid.verify),
	); err != nil {
		ves.Details["id"] = err.Error()
	}

	if err := ozzo.Validate(
		req.Service,
		ozzo.Required,
		ozzo.Length(1, 256),
	); err != nil {
		ves.Details["service"] = err.Error()
	}

	if err := ozzo.Validate(
		req.Method,
		ozzo.Required,
		ozzo.Length(1, 256),
	); err != nil {
		ves.Details["method"] = err.Error()
	}

	if err := ozzo.Validate(
		req.Description,
		ozzo.Required,
		ozzo.Length(1, 256),
	); err != nil {
		ves.Details["description"] = err.Error()
	}

	if len(ves.Details) > 0 {
		return ves
	}

	return nil
}

// Delete deletes permission by id.
func (s *permission) Delete(
	ctx context.Context,
	req *proto.PermissionDeleteRequest,
) (*emptypb.Empty, error) {
	_, err := auth.UserFromContext(ctx)
	if err != nil {
		return nil, stack.Errorf("user from context: %w", err)
	}
	if err := validatePermissionDelete(req); err != nil {
		return nil, stack.Errorf("validate: %w", err)
	}

	if err := s.dl.Delete(ctx, req.Id); err != nil {
		return nil, stack.Errorf("delete: %w", err)
	}

	return new(emptypb.Empty), nil

}

func validatePermissionDelete(req *proto.PermissionDeleteRequest) error {
	ves := validation.NewErrors()
	uuid := uuidVerifier{}

	if err := ozzo.Validate(
		req.Id,
		ozzo.Required,
		ozzo.By(uuid.verify),
	); err != nil {
		ves.Details["id"] = err.Error()
	}

	if len(ves.Details) > 0 {
		return ves
	}

	return nil
}

// Permissions return list of all permissions.
func (s *permission) Permissions(
	ctx context.Context,
	in *emptypb.Empty,
) (*proto.PermissionsResponse, error) {
	_, err := auth.UserFromContext(ctx)
	if err != nil {
		return nil, stack.Errorf("user from context: %w", err)
	}

	permissions, err := s.dl.Permissions(ctx)
	if err != nil {
		return nil, stack.Errorf("permissions: %w", err)
	}

	var pl []*proto.PermissionResponse
	for _, permission := range permissions {
		pl = append(pl, permissionResponse(&permission))
	}

	return &proto.PermissionsResponse{
		Permissions: pl,
	}, nil
}

func permissionResponse(p *domain.PermissionDTO) *proto.PermissionResponse {
	resp := proto.PermissionResponse{
		Id:          p.ID,
		Service:     p.Service,
		Method:      p.Method,
		Description: p.Description,

		Timestamps: &proto.Timestamps{
			Created: timestamppb.New(p.Created),
			Updated: timestamppb.New(p.Updated),
		},
	}

	return &resp
}
