package validation

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"google.golang.org/genproto/googleapis/rpc/errdetails"
)

// Errors holds validation errors.
type Errors struct {
	Message string            `json:"error"`
	Details map[string]string `json:"details"`
}

// NewErrors returns prepared errors.
func NewErrors() Errors {
	e := Errors{
		Message: "you have validation errors",
		Details: make(map[string]string),
	}

	return e
}

// Error implements error interface.
func (v Errors) Error() string {
	return v.Message
}

// Err returns grpc status error.
func (v Errors) Err() error {
	st := status.New(codes.InvalidArgument, v.Message)

	for field, message := range v.Details {
		dt := errdetails.BadRequest_FieldViolation{
			Field:       field,
			Description: message,
		}
		st, _ = st.WithDetails(&dt)
	}
	return st.Err()
}

