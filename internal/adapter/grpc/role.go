package grpc

import (
	"context"
	"errors"

	ozzo "github.com/go-ozzo/ozzo-validation"
	"github.com/romanyx/stack"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/dipress/crmifc/internal/adapter/grpc/validation"
	"gitlab.com/dipress/crmifc/internal/auth"
	"gitlab.com/dipress/crmifc/internal/domain"
	"gitlab.com/dipress/crmifc/internal/proto"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// roleDL role domain logic.
type roleDL interface {
	Create(context.Context, *domain.NewRoleDTO) (*domain.RoleDTO, error)
	Find(context.Context, string) (*domain.RoleDTO, error)
	Update(context.Context, *domain.UpdateRoleDTO) (*domain.RoleDTO, error)
	Delete(context.Context, string) error
	Roles(context.Context) ([]domain.RoleDTO, error)
}

// role server.
type role struct {
	dl roleDL

	proto.UnimplementedRoleServer
}

// Create creates a role.
func (s *role) Create(
	ctx context.Context,
	req *proto.RoleCreateRequest,
) (*proto.RoleResponse, error) {
	_, err := auth.UserFromContext(ctx)
	if err != nil {
		return nil, stack.Errorf("user from context: %w", err)
	}

	if err := validateCreateRole(req); err != nil {
		return nil, stack.Errorf("validate: %w", err)
	}

	var dto domain.NewRoleDTO
	dto.Name = req.Name
	dto.Description = req.Description

	role, err := s.dl.Create(ctx, &dto)
	if err != nil {
		switch {
		case errors.Is(err, domain.ErrNameAlreadyExists):
			return nil, status.Error(
				codes.AlreadyExists,
				err.Error(),
			)
		default:
			return nil, stack.Errorf("create: %w", err)
		}
	}

	return roleResponse(role), nil
}

func validateCreateRole(req *proto.RoleCreateRequest) error {
	ves := validation.NewErrors()

	if err := ozzo.Validate(
		req.Name,
		ozzo.Required,
		ozzo.Length(1, 256),
	); err != nil {
		ves.Details["name"] = err.Error()
	}

	if err := ozzo.Validate(
		req.Description,
		ozzo.Required,
		ozzo.Length(1, 256),
	); err != nil {
		ves.Details["description"] = err.Error()
	}

	if len(ves.Details) > 0 {
		return ves
	}

	return nil
}

// Find finds role by id.
func (s *role) Find(
	ctx context.Context,
	req *proto.RoleFindRequest,
) (*proto.RoleResponse, error) {
	_, err := auth.UserFromContext(ctx)
	if err != nil {
		return nil, stack.Errorf("user from context: %w", err)
	}

	if err := validateFindRole(req); err != nil {
		return nil, stack.Errorf("validate: %w", err)
	}

	role, err := s.dl.Find(ctx, req.Id)
	if err != nil {
		return nil, stack.Errorf("find: %w", err)
	}

	return roleResponse(role), nil
}

func validateFindRole(req *proto.RoleFindRequest) error {
	ves := validation.NewErrors()
	uuid := uuidVerifier{}

	if err := ozzo.Validate(
		req.Id,
		ozzo.Required,
		ozzo.By(uuid.verify),
	); err != nil {
		ves.Details["id"] = err.Error()
	}

	if len(ves.Details) > 0 {
		return ves
	}

	return nil
}

// Update updates role by id.
func (s *role) Update(
	ctx context.Context,
	req *proto.RoleUpdateRequest,
) (*proto.RoleResponse, error) {
	_, err := auth.UserFromContext(ctx)
	if err != nil {
		return nil, stack.Errorf("user from context: %w", err)
	}

	if err := validateRoleUpdate(req); err != nil {
		return nil, stack.Errorf("validate: %w", err)
	}

	var dto domain.UpdateRoleDTO
	dto.ID = req.Id
	dto.Name = req.Name
	dto.Description = req.Description

	role, err := s.dl.Update(ctx, &dto)
	if err != nil {
		return nil, stack.Errorf("update: %w", err)
	}

	return roleResponse(role), nil
}

func validateRoleUpdate(req *proto.RoleUpdateRequest) error {
	ves := validation.NewErrors()
	uuid := uuidVerifier{}

	if err := ozzo.Validate(
		req.Id,
		ozzo.Required,
		ozzo.By(uuid.verify),
	); err != nil {
		ves.Details["id"] = err.Error()
	}

	if err := ozzo.Validate(
		req.Name,
		ozzo.Required,
		ozzo.Length(1, 256),
	); err != nil {
		ves.Details["name"] = err.Error()
	}

	if err := ozzo.Validate(
		req.Description,
		ozzo.Required,
		ozzo.Length(1, 256),
	); err != nil {
		ves.Details["description"] = err.Error()
	}

	if len(ves.Details) > 0 {
		return ves
	}

	return nil
}

// Delete deletes role by id.
func (s *role) Delete(
	ctx context.Context,
	req *proto.RoleDeleteRequest,
) (*emptypb.Empty, error) {
	_, err := auth.UserFromContext(ctx)
	if err != nil {
		return nil, stack.Errorf("user from context: %w", err)
	}
	if err := validateRoleDelete(req); err != nil {
		return nil, stack.Errorf("validate: %w", err)
	}

	if err := s.dl.Delete(ctx, req.Id); err != nil {
		return nil, stack.Errorf("delete: %w", err)
	}

	return new(emptypb.Empty), nil

}

func validateRoleDelete(req *proto.RoleDeleteRequest) error {
	ves := validation.NewErrors()
	uuid := uuidVerifier{}

	if err := ozzo.Validate(
		req.Id,
		ozzo.Required,
		ozzo.By(uuid.verify),
	); err != nil {
		ves.Details["id"] = err.Error()
	}

	if len(ves.Details) > 0 {
		return ves
	}

	return nil
}

// Roles return list of all roles.
func (s *role) Roles(
	ctx context.Context,
	in *emptypb.Empty,
) (*proto.RolesResponse, error) {
	_, err := auth.UserFromContext(ctx)
	if err != nil {
		return nil, stack.Errorf("user from context: %w", err)
	}

	roles, err := s.dl.Roles(ctx)
	if err != nil {
		return nil, stack.Errorf("roles: %w", err)
	}

	var rl []*proto.RoleResponse
	for _, role := range roles {
		rl = append(rl, roleResponse(&role))
	}

	return &proto.RolesResponse{
		Roles: rl,
	}, nil
}

func roleResponse(r *domain.RoleDTO) *proto.RoleResponse {
	resp := proto.RoleResponse{
		Id:          r.ID,
		Name:        r.Name,
		Description: r.Description,
		Timestamps: &proto.Timestamps{
			Created: timestamppb.New(r.Created),
			Updated: timestamppb.New(r.Updated),
		},
	}

	return &resp
}

type uuidVerifier struct{}

// nolint: gocyclo
func (u *uuidVerifier) verify(value interface{}) error {
	id, ok := value.(string)
	if !ok {
		return errors.New("value must be a string")
	}

	_, err := uuid.FromString(id)
	if err != nil {
		return errors.New("id is invalid")
	}

	return nil
}
