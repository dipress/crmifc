package grpc

import (
	"database/sql"
	"net"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"gitlab.com/dipress/crmifc/internal/auth"
	"gitlab.com/dipress/crmifc/internal/domain"
	"gitlab.com/dipress/crmifc/internal/proto"
	"gitlab.com/dipress/crmifc/internal/storage/postgres"
	"google.golang.org/grpc"
)

// Logger allows to log server.
type Logger interface {
	Info(info string, extra map[string]interface{})
	Warn(warn string, extra map[string]interface{})
	Error(err error, extra map[string]interface{})
	Fatal(err error, extra map[string]interface{})
}

// Server allows to stop and start grpc server.
type Server struct {
	Addr string
	lis  net.Listener
	base *grpc.Server
}

// Start starts server.
func (s *Server) Start() error {
	return s.base.Serve(s.lis) // nolint: errcheck
}

// Shutdown stops server.
func (s *Server) Shutdown() {
	s.base.GracefulStop()
}

func (s *Server) Address() string {
	return s.Addr
}

// NewServer registers server.
func NewServer(
	listener net.Listener,
	db *sql.DB,
	a *auth.Authenticator,
	logger Logger,
) *Server {
	// DAOs
	userDAO := postgres.NewUser(db)
	roleDAO := postgres.NewRole(db)
	permissionDAO := postgres.NewPermission(db)
	admissionDAO := postgres.NewAdmission(db)

	// DLs
	authDL := domain.NewAuth(userDAO, a)
	userDL := domain.NewUser(userDAO, roleDAO, a)
	roleDL := domain.NewRole(roleDAO)
	permissionDL := domain.NewPermission(permissionDAO)
	admissionDL := domain.NewAdmission(admissionDAO)

	// Unary Interceptors
	var unaryInterceptors []grpc.UnaryServerInterceptor
	unaryInterceptors = append(unaryInterceptors,
		logUnaryInterceptor(logger),
		authUnaryInterceptor(a, userDAO),
	)

	// Options
	opts := []grpc.ServerOption{
		grpc.UnaryInterceptor(
			grpc_middleware.ChainUnaryServer(
				unaryInterceptors...,
			),
		),
	}

	s := grpc.NewServer(opts...)

	proto.RegisterAuthServer(s, &authentication{
		dl: authDL,
	})

	proto.RegisterRoleServer(s, &role{
		dl: roleDL,
	})

	proto.RegisterPermissionServer(s, &permission{
		dl: permissionDL,
	})

	proto.RegisterUserServer(s, &user{
		dl: userDL,
	})

	proto.RegisterAdmissionServer(s, &admission{
		dl: admissionDL,
	})

	srv := Server{
		base: s,
		lis:  listener,
		Addr: listener.Addr().String(),
	}

	return &srv

}
